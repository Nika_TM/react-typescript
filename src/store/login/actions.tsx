import { SET_AUTH } from './constants';

export const setAuth = (canAuth: any) => {
    return {
        type: SET_AUTH,
        payload: canAuth
    };
};