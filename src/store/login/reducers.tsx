import { SET_AUTH } from './constants';
import { SET_MODAL } from '../modal/constants';
import { SET__SELECETED_ROW } from '../modal/drivers/constants';
import { combineReducers } from 'redux';

const initialState = {
  user: {
    canAuth: false,
    token: ''
  },
  showModal: false,

  selectedRow: null
};

const user = (state = initialState.user, action: any) => {
  switch (action.type) {
      case SET_AUTH:
          return {
              ...state,
              ...action.payload
          };

      default:
          return state;
  }
};

const showModal = (state: any = initialState.showModal, action: any)  => {
  switch (action.type) {
    case SET_MODAL:
    return {
      ...state,
      ...action.payload
    };

    default:
      return state;
  }
};

const selectedRow = (state: any = initialState.selectedRow, action: any)  => {
  switch (action.type) {
    case SET__SELECETED_ROW:
    return {
      ...state,
      ...action.payload
    };

    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  showModal,
  user,
  selectedRow
});
