export const loadState = () => {
  try {
      const serializeState = localStorage.getItem('storage');
      if (serializeState === null) {
          return undefined;
      }
      return JSON.parse(serializeState);
  } catch (err) {
      return undefined;
  }
};

export const saveState = (state: any) => {
    try {
       const serializedState = JSON.stringify(state);
       localStorage.setItem('storage', serializedState);
    } catch (err) {
       console.log(err);
    }
};