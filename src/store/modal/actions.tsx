import { SET_MODAL } from './constants';

export const setModal = (showModal: any) => {
    return {
        type: SET_MODAL,
        payload: showModal
    };
};
