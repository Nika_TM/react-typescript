import { SET__SELECETED_ROW } from './constants';

export const getRowData = (rowData: any) => {
    return {
        type: SET__SELECETED_ROW,
        payload: rowData
    };
};
