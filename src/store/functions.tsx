import  { bindActionCreators } from 'redux';
import { setAuth } from './login/actions';
import { setModal } from './modal/actions';
import { getRowData } from './modal/drivers/actions';

export const putStateProps = (state: any) => {
    return {
        ...state,
    };
};

export const putActionsToProps = (dispatch: any) => {
    return {
        setAuth: bindActionCreators(setAuth, dispatch),
        setModal: bindActionCreators(setModal, dispatch),
        getRowData: bindActionCreators(getRowData, dispatch),
    };
};
