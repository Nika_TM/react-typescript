import * as React from 'react';
import * as ReactDOM from 'react-dom';

import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';

import { rootReducer } from './store/login/reducers';

import { loadState, saveState } from './store/localstorage/';
import { composeWithDevTools } from 'redux-devtools-extension';

import AuthInit from './components/routes';
import Footer from './components/footer';

const persistedState = loadState(); 

import 'react-toastify/dist/ReactToastify.css';
import 'react-virtualized-select/styles.css';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import './assets/style/App.css';

const store = createStore(
    rootReducer,
    persistedState, 
    composeWithDevTools(
        applyMiddleware()
    )
);

store.subscribe(() => {
    saveState({
        user: store.getState().user
    });
});

const render = (Component: any) => {
    ReactDOM.render(
        <React.Fragment>
            <Provider store={store}>
                <Component />
            </Provider>
            <Footer />
        </React.Fragment>,
        document.getElementById('root')
    );
};

if ((module as any).hot) {
    // Enable Webpack hot module replacement for reducers
    (module as any).hot.accept('./components/routes', () => {
        const NextRootContainer = require('./components/routes').default;
        render(NextRootContainer);
    });
}

render(AuthInit);
