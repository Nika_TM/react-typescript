import * as React from 'react';
import { connect } from 'react-redux';
import { TableHeaderColumn } from 'react-bootstrap-table';

import { putActionsToProps, putStateProps } from '../../store/functions';
import FORMAT from '../modules/table/tableFormats';
import { serverAction, ACTION_TYPES } from '../../scripts/actions';
import BTable from '../modules/table';
import { SyncLoader } from 'react-spinners';

class Table extends React.Component<any, any> {
    state: any = {
        tableData: [],
        loader: true
    };

    componentDidMount() {
        serverAction(ACTION_TYPES.CONTRACT.LIST, null).then((tableData: any) => {
            this.setState({
                tableData,
                loader: false
            });
        });
    }

    dataLoad = () => {
        if (this.state.loader) {
            return (
                <SyncLoader
                    color={'#5898c6'}
                    loading={this.state.loader}
                    size={10}
                />
            );
        } else {
            return (
                'Нет маршрутов'
            );
        }
    }

    render() {
        return (
            <BTable
                data={this.state.tableData}
                options={{
                    noDataText: this.dataLoad()
                }}
            >
                <TableHeaderColumn
                    dataField="id"
                    width="0"
                    columnClassName={FORMAT.ROW_CLASS}
                    hidden={true}
                    isKey={true}
                >
                    Статус
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="name"
                    width="120"
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    columnClassName={FORMAT.ROW_CLASS}
                >
                    Контракт
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="firm"
                    width="120"
                    dataFormat={FORMAT.OBJECT}
                    filterFormatted={true}
                    columnClassName={FORMAT.ROW_CLASS}
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                >
                    Фирма
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="status"
                    dataFormat={FORMAT.CONTRACT_STATUS}
                    width="120"
                    columnClassName={FORMAT.ROW_CLASS}
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                >
                    Статус
                </TableHeaderColumn>
            </BTable>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(Table);
