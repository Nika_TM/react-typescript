import * as React from 'react';
import Header from '../header/index';
import Table from './table';

class Contracts extends React.Component<any, any> {
    render(): any {
        return (
            <React.Fragment>
              <Header/>
              <div className="col-md-12" style={{padding: 0}}>
                  <div className="col-md-12 zt--Container">
                      <Table/>
                  </div>
              </div>
            </React.Fragment>
        );
    }
}

export default Contracts;
