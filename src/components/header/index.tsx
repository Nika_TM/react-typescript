import * as React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { ToastContainer } from 'react-toastify';
import { connect } from 'react-redux';

import { putActionsToProps, putStateProps } from '../../store/functions';
const dashLogo = require('../../assets/images/dashLogo.png');
import { logOut } from '../logout/';

class Header extends React.Component<any, any> {
    clickLogOut = () => logOut(this.props);

    render() {
        return (
            <React.Fragment>
                <Navbar inverse={true} collapseOnSelect={true} className="header-container">
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="javascript:void(0)"><img src={dashLogo}/></a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav>
                            <LinkContainer
                                to="/roads"
                                activeClassName="active"
                            >
                              <NavItem eventKey={1}>Маршруты</NavItem>
                            </LinkContainer>

                            <LinkContainer
                                activeClassName="active"
                                to="/history"
                            >
                              <NavItem eventKey={2}>История</NavItem>
                            </LinkContainer>

                            <LinkContainer
                                activeClassName="active"
                                to="/map"
                            >
                              <NavItem eventKey={4}>Карта</NavItem>
                            </LinkContainer>

                            <LinkContainer
                                activeClassName="active"
                                to="/contracts"
                            >
                              <NavItem eventKey={3}>Контракты</NavItem>
                            </LinkContainer>

                            <LinkContainer
                                activeClassName="active"
                                to="/transport"
                            >
                              <NavItem eventKey={3}>Автохозяйства</NavItem>
                            </LinkContainer>

                            {/* <LinkContainer
                                activeClassName="active"
                                to="/dispatchers"
                            >
                              <NavItem eventKey={5}>Диспетчеры</NavItem>
                            </LinkContainer> */}
                        </Nav>
                        <Nav pullRight={true}>
                            <NavItem eventKey={6} onClick={this.clickLogOut}>
                                Выход<i className="exit--Icon"/>
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <ToastContainer/>
            </React.Fragment>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(Header);
