import * as React from 'react';
import Select from 'react-virtualized-select';
import PointSelect from 'react-virtualized-select';

export class SelectSender extends React.Component <any, any> {
  public static defaultProps = {
    regions: [],
    senders: [],
    points: [],
    sender: {},
  };

  public state: any = {
    regions: this.props.regions,
    points: this.props.sender
    && this.props.sender.region
    && this.props.sender.region.points,
    region: this.props.sender && this.props.sender.region,
    point: this.props.sender && this.props.sender.point,
  };

  static getDerivedStateFromProps(nextProps: any, prevState: any) {
    return {
      regions: nextProps.regions,
    };
  }

  handleRegion = (region: any) => {
    if (region) {
      this.setState({
        region,
        point: null,
        points: region.points,
      }, () => {
        this.props.handleChange('sender', {
          region,
          point: this.state.point,
        });
      });
    } else {
      this.setState({
        region: null,
        point: null,
        points: [],
      }, () => {
        this.props.handleChange('sender', {
          region: null,
          point: null,
        });
      });
    }
  }

  handlePoint = (point: any) => {
    if (point) {
      this.setState({
        point
      }, () => {
        this.props.handleChange('sender', {
          point,
          region: this.state.region,
        });
      });
    } else {
      this.setState({
        point: null
      }, () => {
        this.props.handleChange('sender', {
          point: null,
          region: this.state.region,
        });
      });
    }
  }

  render() {
    let points = [];
    if (this.state.region && this.state.regions && this.state.regions.length > 0) {
      points = this.state.regions.filter((region: any) => this.state.region.uid === region.uid)[0].points;
    }
    return(
      <div className="col-md-12">
        <div className="col-md-6">
          <Select
            name="region"
            className={'zt--select'}
            placeholder="Регион отправителя..."
            options={this.state.regions}
            onChange={this.handleRegion}
            labelKey="name"
            valueKey="name"
            noResultsText="Результатов нет"
            value={this.state.region}
            disabled={this.props.disabled}
          />
        </div>

        <div className="col-md-6">
          <PointSelect
            name="senderPoint"
            className="zt--select sender-select pull-right"
            placeholder="Пункт погрузки"
            disabled={this.props.disabled || !this.state.region}
            options={points}
            onChange={this.handlePoint}
            labelKey="name"
            valueKey="name"
            noResultsText="Результатов нет"
            value={this.state.point}
          />
        </div>
      </div>
    );
  }
}
