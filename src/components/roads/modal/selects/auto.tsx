import * as React from 'react';
import Select from 'react-virtualized-select';
import { Button } from 'react-bootstrap';

const NO_CARRIER_UID = '008aee99-59a6-11e8-aac2-6466b304e311';
const NO_DRIVER_UID = '0c960260-59a6-11e8-aac2-6466b304e311';

export class AutoSelect extends React.Component <any, any> {
   public static defaultProps = {
    selectedAutos: [],
    driversList: [],
    autosList: [],
    carriers: [],
    roadType: 0,
  };

  state = {
    selectedAutos: this.props.selectedAutos,
    driversList: this.props.driversList,
    autosList: this.props.autosList,
  };

  static getDerivedStateFromProps(nextProps: any) {
    return {
      autosList: nextProps.autosList,
      driversList: nextProps.driversList,
      selectedAutos: nextProps.selectedAutos,
    };
  }

  setDriver = (index: number, selectedDriver: any) => {
    const { selectedAutos } = this.state;
    
    selectedAutos[index].driver = selectedDriver;
    this.setState({
      selectedAutos,
    }, () => {
      this.props.handleChange('selectedAutos', selectedAutos);
    });
  }

  setAuto = (index: number, selectedAuto: any) => {
    const { selectedAutos } = this.state;
    selectedAutos[index].auto = selectedAuto;
    this.setState({
      selectedAutos,
    }, () => {
      this.props.handleChange('selectedAutos', selectedAutos);
    });
  }

  handleAddDriver = () => {
    const { selectedAutos } = this.state;
    this.setState({
      selectedAutos: [{}, ...selectedAutos]
    });
  }

  handleRemove = (index: number) => {
    let { selectedAutos } = this.state;
    selectedAutos.splice(index, 1);
    this.setState({
      selectedAutos,
    }, () => {
      this.props.handleChange('selectedAutos', selectedAutos);
    });
  }

  handleCarrier = (index: number, selectedCarrier: any) => {
    const { selectedAutos } = this.state;
    
    selectedAutos[index].carrier = selectedCarrier;
    selectedAutos[index].driver = null;
    selectedAutos[index].auto = null;
    this.setState({
      selectedAutos,
    }, () => {
      this.props.handleChange('selectedAutos', selectedAutos);
    });
  }

  autoComponent = (transport: any, index: number) => (
    <div className="col-md-11 autoContainer" style={{ margin: '20px 0' }} key={`driver${index}`}>
      <div className="row">
        <Select
          onChange={this.handleCarrier.bind(null, index)}
          className="zt--select"
          placeholder="Компания перевозчик..."
          noResultsText="Результатов нет"
          labelKey="name"
          valueKey="name"
          name="name"
          options={this.props.roadType === 2
            ? this.props.carriers : this.props.carriers.filter((driver: any) => driver.uid
            !== NO_CARRIER_UID)}
          value={transport && transport.carrier}
        />
      </div>

      <div className="row" style={{ margin: '10px -15px' }} >
        <Select
          className="zt--select"
          placeholder="Выберите номер машины"
          noResultsText="Результатов нет"
          options={transport
            && transport.carrier
            && this.props.carriers
              .find((item: any) => item.uid === transport.carrier.uid)
              &&
              this.props.carriers
              .find((item: any) => item.uid === transport.carrier.uid)
              .autos.map((option: any) => {
                return {
                  ...option,
                  name: `${option.name} - ${option.model_name}`
                };
          }) || []}
          onChange={this.setAuto.bind(null, index)}
          labelKey="name"
          value={transport && transport.auto}
        />
      </div>

      <div className="row">
        <Select
          placeholder="Выберите водителя на маршрут"
          noResultsText="Результатов нет"
          onChange={this.setDriver.bind(null, index)}
          className="zt--select"
          options={this.props.roadType === 2
            ? this.state.driversList
            : this.state.driversList.filter((driver: any) => driver.uid
            !== NO_DRIVER_UID)}
          value={transport && transport.driver}
          labelKey="name"
          disabled={!transport}
        />
      </div>

      <button
        className="deleteButton"
        disabled={this.state.selectedAutos.length < 2}
        onClick={this.handleRemove.bind(null, index)}
      />
    </div>
)

  render() {
    return(
      <div className="col-md-12">
         <Button
           bsStyle="success"
           bsSize="large"
           block={true}
           onClick={this.handleAddDriver}
           disabled={this.props.disabled}
         >
           Добавить автомобиль
         </Button>
        <div className="col-md-12">
          {
            this.state.selectedAutos
              .map((auto: any, index: any) => this.autoComponent(auto, index))
          }
        </div>
      </div>
    );
  }
}
