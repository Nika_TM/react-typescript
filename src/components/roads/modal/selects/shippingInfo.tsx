import * as React from 'react';
import FormInput from '../../../modules/form/formInput';

export const ShippingInformation = (props: any) => (
  <React.Fragment>
    <div className="col-md-12 weight">
      <div className="col-md-6">
        <FormInput
          type="text"
          className="zt--input"
          value={props.loadingWeight}
          name="loadingWeight"
          onChange={props.handleChange}
          placeholder="Вес погрузки..."
          disabled={props.disabled}
        />
      </div>

      <div className="col-md-6">
        <FormInput
          className="pull-right zt--input"
          type="number"
          placeholder="Вес выгрузки..."
          value={props.unloadingWeight}
          name="unloadingWeight"
          onChange={props.handleChange}
          disabled={props.disabled}
        />
      </div>
    </div>

    <div className="col-md-12">
        <FormInput
          type="text"
          className="zt--input"
          placeholder="Номер ТТН..."
          value={props.ttnNum}
          onChange={props.handleChange}
          name="ttnNum"
          disabled={props.disabled}
        />
    </div>

    <div className="col-md-12">
        <FormInput
          type="text"
          className="zt--input"
          placeholder="Цена доставки..."
          value={props.deliveryPrice}
          onChange={props.handleChange}
          name="deliveryPrice"
          disabled={props.disabled}
        />
    </div>
  </React.Fragment>
);
