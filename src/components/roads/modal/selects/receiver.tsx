import * as React from 'react';
import Select from 'react-virtualized-select';
import PointSelect from 'react-virtualized-select';

export class SelectReceiver extends React.Component <any, any> {
  public static defaultProps = {
    receivers: [],
    receiver: {}
  };

  public state: any = {
    regions: this.props.regions,
    points: this.props.points && this.props.receiver.region && this.props.receiver.region.points 
    && this.props.default_point,
    default_region: '',
    default_point: '',
  };

  static getDerivedStateFromProps(nextProps: any, prevState: any) {
    return {
      default_region: nextProps.receiver && nextProps.receiver.region,
      default_point: nextProps.receiver && nextProps.receiver.point,
      regions: nextProps.regions,
      defPoints: nextProps.initInfo.receiver_pointsDeff,
      receiverRegionPoint: nextProps.initInfo.roadData
    };
  }

  handleRegion = (region: any) => {
    if (region) {
      this.setState({
        region,
        point: null,
        points: region.points,
      }, () => {
        this.props.handleChange('receiver', {
          region,
          point: this.state.point,
        });
      });
    } else {
      this.setState({
        region,
        point: null,
        points: [],
      }, () => {
        this.props.handleChange('receiver', {
          region: null,
          point: null,
        });
      });
    }
  }

  handlePoint = (point: any) => {
    if (point) {
      this.setState({
        point
      }, () => {
        this.props.handleChange('receiver', {
          region: this.state.region,
          point: this.state.point,
        });
      });
    } else {
      this.setState({
        point: null
      }, () => {
        this.props.handleChange('receiver', {
          region: this.state.region,
          point: null,
        });
      });
    }
  }

  render() {
    let points = [];
    if (this.state.region && this.state.regions && this.state.regions.length > 0) {
      points = this.state.regions.filter((region: any) => this.state.region.uid === region.uid)[0].points;
    } else {
      points = this.state.points;
    }

    return (
      <div className="col-md-12">
        <div className="col-md-6">
          <Select
            name="region"
            className={'zt--select'}
            placeholder="Регион получателя..."
            options={this.state.regions}
            onChange={this.handleRegion}
            labelKey="name"
            valueKey="name"
            noResultsText="Результатов нет"
            value={this.state.default_region}
            disabled={this.props.disabled}
          />
        </div>

        <div className="col-md-6">
          <PointSelect
            name="senderPoint"
            className="zt--select sender-select pull-right"
            placeholder="Пункт выгрузки"
            disabled={this.props.disabled || !this.state.default_region && !this.state.region}
            options={points}
            onChange={this.handlePoint}
            labelKey="name"
            valueKey="name"
            noResultsText="Результатов нет"
            value={this.state.default_point}
          />
        </div>
      </div>
    );
  }
}
