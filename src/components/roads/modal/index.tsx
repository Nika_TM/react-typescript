import * as React from 'react';
import { ButtonToolbar, Button, Modal, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import Select from 'react-virtualized-select';
import { connect } from 'react-redux';

import { putStateProps, putActionsToProps } from '../../../store/functions';
import { serverAction, ACTION_TYPES } from '../../../scripts/actions';
import { MODAL_TYPE, ROAD_TYPE } from '../../../scripts/constants';
import { ShippingInformation } from './selects/shippingInfo';
import FormInput from '../../modules/form/formInput';
import { SelectReceiver } from './selects/receiver';
import { getErrors } from '../../modules/errors';
import { SelectSender } from './selects/sender';
import { AutoSelect } from './selects/auto';

const defaultRoadData: any = {
  roadType: ROAD_TYPE.EXW,
  unloadingWeight: '',
  loadingWeight: '',
  deliveryPrice: '',
  selectedAutos: [],
  receiver: null,
  contract: null,
  disabled: true,
  modalInfo: {},
  carrier: null,
  sender: null,
  culture: {},
  ttnNum: '',
  firm: {},
  receiver_region: '',
  receiver_point: '',
  transit: false,
  transitContract: ''
};

class ModalForm extends React.Component<any, any, any> {
  static defaultProps = {
    choosenRow: {},
    modalInfo: {},
    isEdit: false,
    showModal: false
  };

  state: any = {
    roadData: { ...defaultRoadData },
    modalInfo: [],
    transit: false,
  };

  static getDerivedStateFromProps(nextProps: any, prevState: any) {
    const { choosenRow, modalInfo, isEdit, addDriver }: any = nextProps;

    let roadData = prevState.roadData;

    if (isEdit) {
      roadData = {
        road_id: choosenRow.id,
        ttnNum: choosenRow.ttn_number,
        deliveryPrice: choosenRow.delivery_price,
        selectedAutos: [{
          auto: choosenRow.auto,
          driver: choosenRow.driver,
          carrier: {
            ...choosenRow.carrier || '',
            auto: {
              ...choosenRow.auto || '',
              driver: {
                ...choosenRow.driver || ''
              },
            }
          },
        }],
        unloadingWeight: choosenRow.unloading_weight,
        loadingWeight: choosenRow.loading_weight,
        receiver: {
          region: choosenRow.receiver_region,
          point: choosenRow.receiver_point,
        },
        sender: {
          region: choosenRow.sender_region,
          point: choosenRow.sender_point,
        },
        contract: {
          ...choosenRow.contract,
          contract_name: choosenRow.contract
            && choosenRow.contract.name,
        },
        transitContract: {
          ...choosenRow.transit_contract,
        },
        culture: choosenRow.culture,
        roadType: choosenRow.road_type,
        firm: choosenRow.firm,
        modalInfo: {},
      };

    }

    if (!nextProps.showModal.show) {
      roadData = defaultRoadData;
    }

    const isDisabled = () => {
      if (!nextProps.showModal.show) {
        return false;
      } else if (!isEdit && !!prevState.roadData.contract) {
        return false;
      } else if (isEdit && addDriver) {
        return true;
      } else if (isEdit) {
        return false;
      } else if (prevState.disabled ) {
        return true;
      }
      return true;
    };

    return {
      disabled: isDisabled(),
      addDriver,
      roadData: { ...roadData },
      modalInfo,
      transit: isEdit ? !!choosenRow.transit_contract : false,
    };
  }

  validate = (data: any) => {
    let isError = false;
    for (let key in data) {

      let error = getErrors({
        type: MODAL_TYPE.ROADS,
        fieldName: key,
        value: data[key]
      });
      if (error) {

        isError = true;
      }
    }
    return isError;
  }

  handleSave = () => {
    const { roadData } = this.state;
    const isError = this.validate(roadData);
    if (!isError) {
      const data = this.prepareData(roadData);
      let actionName = '';
      if (!this.props.isEdit) {
        actionName = ACTION_TYPES.ROAD.CREATE;
      } else if (this.props.isEdit
        && this.state.roadData.selectedAutos
        && this.state.roadData.selectedAutos.length > 1) {
        actionName = ACTION_TYPES.ROAD.UPADATE_LIST;
        data.id = null;
      } else if (this.props.isEdit) {
        actionName = ACTION_TYPES.ROAD.UPDATE;
      }

      serverAction(actionName, data)
        .then((resData: any) => {
          this.setState({
            ...defaultRoadData,
          }, () => {
            this.props.setModal({ show: false });
          });
        });
    }
  }

  prepareData = (roadData: any) => {
    const autos = roadData.selectedAutos.map((transport: any) => {
      if (transport) {
        return {
          driver_uid: transport.driver.uid,
          auto_uid: transport.auto.uid,
          carrier_uid: transport.carrier.uid,
        };
      }
      return null;
    });

    return {
      id: roadData.road_id,
      ttn_num: roadData.ttnNum,
      delivery_price: roadData.deliveryPrice,
      autos,
      unloading_weight: roadData.unloadingWeight,
      loading_weight: roadData.loadingWeight,
      receiver_point_uid: roadData.receiver.point.uid,
      receiver_region_uid: roadData.receiver.region.uid,
      sender_point_uid: roadData.sender.point.uid,
      sender_region_uid: roadData.sender.region.uid,
      contract_uid: roadData.contract.uid,
      road_type: roadData.roadType,
      transit_contract: roadData.transitContract && roadData.transitContract.uid,
    };
  }

  handleHide = () => {
    this.setState({
      ...defaultRoadData,
    }, () => {
      this.props.setModal({
        show: false,
      });
    });
  }

  handleChange = (name: any, value: any) => {
    this.setState({
      roadData: {
        ...this.state.roadData,
        [name]: value,
      },
    });
  }

  handleTransitContract = (contract: any) => {
    if (contract) {
      for (let i in this.state.modalInfo.regions) {
        if (contract.receiver_region.uid === this.state.modalInfo.regions[i].uid) {
          this.setState({
            roadData: {
              ...this.state.roadData,
              transitContract: contract
            }
          });
        }
      }
    } else {
      this.setState({
        roadData: {
          ...defaultRoadData,
        }
      });
    }
  }

  handleContract = (contract: any) => {
    if (contract) {
      for (let i in this.state.modalInfo.regions) {
        if (contract.receiver_region.uid === this.state.modalInfo.regions[i].uid) {
          this.setState({
            disabled: false,
            roadData: {
              ...this.state.roadData,
              contract,
              culture: contract.culture,
              firm: contract.firm,
              receiver: {
                region: contract.receiver_region,
                point: contract.receiver_point,
              },
            }
          });
        }
      }
    } else {
      this.setState({
        disabled: true,
        roadData: {
          ...defaultRoadData,
        }
      });
    }
  }

  handleRoadType = (roadType: any) => {
    this.setState({
      roadData: {
        ...this.state.roadData,
        roadType,
        selectedAutos: [],
      }
    });
  }

  handleCarrier = (carrier: any) => {
    this.setState({
      roadData: {
        ...this.state.roadData,
        carrier,
        selectedAutos: [],
      }
    });
  }

  setTransit = () => {
    if (this.state.transit === false) {
      this.setState({
        transit: true
      });
    } else {
      this.setState({
        transit: false
      });
    }
  }

  render() {
    const { roadData, modalInfo, disabled } = this.state;
    let autos = [];

    if (roadData.carrier && modalInfo && modalInfo.carriers) {
      const data = modalInfo.carriers.filter((item: any) => item.uid === roadData.carrier.uid);
      autos = data[0].autos;
    }
    return (
      <ButtonToolbar>
        <Modal
          show={this.props.showModal.show}
          onHide={this.handleHide}
          dialogClassName="zt-modal"
        >
          <Modal.Header closeButton={false}>
            <Modal.Title id="contained-modal-title-lg">
              Создание маршрута
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className="col-md-12">
            <div className="col-md-8">
              <h4>Маршрут</h4>
              <div className="col-md-12">
                <ButtonToolbar
                  bsStyle="primary"
                  className="road-type-buttons pull-left"
                  style={{ margin: '10px 0' }}
                >
                  <ToggleButtonGroup
                    onChange={this.handleRoadType}
                    type="radio"
                    name="options"
                    defaultValue={1}
                  >
                    <ToggleButton
                      disabled={this.props.isEdit || !this.state.roadData.contract}
                      value={ROAD_TYPE.EXW}
                      className="btn-grey"
                    >EXW
                    </ToggleButton>
                    <ToggleButton
                      disabled={this.props.isEdit || !this.state.roadData.contract}
                      value={ROAD_TYPE.SPT}
                      className="btn-grey"
                    >SPT
                    </ToggleButton>

                    <button 
                      className={this.state.transit ? 
                        'containerForCheckbox green' : 'containerForCheckbox grey'} 
                      onClick={this.setTransit}
                    >
                        Транзит
                    </button>

                  </ToggleButtonGroup>
                </ButtonToolbar>
              </div>

              <div className={this.state.transit ? 'col-md-12 transitZone showed' : 'hidden'}>
                <div className="col-md-6">
                  <Select
                    placeholder="Контракт по Транзиту"
                    noResultsText="Результатов нет"
                    onChange={this.handleTransitContract}
                    options={modalInfo.contracts}
                    value={roadData.transitContract || null}
                    className="zt--select"
                    labelKey="name"
                    valueKey="name"
                    name="name"
                  />
                </div>
              </div>

              <div className="col-md-12">
                <div className="col-md-6">
                  <Select
                    placeholder="Номер контракта..."
                    noResultsText="Результатов нет"
                    onChange={this.handleContract}
                    options={modalInfo.contracts}
                    value={roadData.contract || null}
                    className="zt--select"
                    labelKey="name"
                    valueKey="name"
                    name="name"
                    disabled={this.state.addDriver}
                  />
                </div>
              </div>

              <SelectSender
                handleChange={this.handleChange}
                regions={modalInfo.regions}
                sender={roadData.sender}
                disabled={disabled}
              />

              <SelectReceiver
                handleChange={this.handleChange}
                regions={modalInfo.regions}
                receiver={roadData.receiver}
                disabled={this.state.disabled}
                initInfo={this.state}
              />

              <div className="col-md-12">
                <div className="col-md-6">
                  <FormInput
                    name="culture"
                    value={roadData.culture && roadData.culture.name || ''}
                    placeholder={'Культура'}
                    disabled={true}
                  />
                </div>
              </div>

              <div className="col-md-12">
                <div className="col-md-6">
                  <FormInput
                    name="firm"
                    value={roadData.firm && roadData.firm.name || ''}
                    placeholder={'Фирма'}
                    disabled={true}
                  />
                </div>
              </div>

              <ShippingInformation
                unloadingWeight={roadData.unloadingWeight}
                loadingWeight={roadData.loadingWeight}
                deliveryPrice={roadData.deliveryPrice}
                handleChange={this.handleChange}
                ttnNum={roadData.ttnNum}
                disabled={disabled}
              />
            </div>

            <div className="col-md-4">
              <span className="separator" />
              <h4>Транспорт</h4>
              <AutoSelect
                selectedAutos={roadData.selectedAutos}
                roadType={roadData.roadType}
                autosList={autos}
                carriers={modalInfo.carriers}
                handleChange={this.handleChange}
                isEdit={this.props.isEdit}
                disabled={this.props.isEdit && !this.state.addDriver
                  || !this.state.roadData.contract}
                driversList={modalInfo.drivers}
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button className="close--btn pull-right" onClick={this.handleHide}>Отменить</Button>
            <Button className="save--btn pull-right" onClick={this.handleSave}>Сохранить</Button>
          </Modal.Footer>
        </Modal>
      </ButtonToolbar>
    );
  }
}

export default connect<any, any, any>(putStateProps, putActionsToProps)(ModalForm);
