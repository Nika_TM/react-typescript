import * as React from 'react';
import { connect } from 'react-redux';
import { SyncLoader } from 'react-spinners';
import { MenuItem } from 'react-bootstrap';
import { TableHeaderColumn } from 'react-bootstrap-table';

import { putActionsToProps, putStateProps } from '../../store/functions';
import { serverAction, ACTION_TYPES } from '../../scripts/actions';
import { store, STORE_LIST } from '../../scripts/store';

import checkbox from '../modules/table/tableCheckbox';
import Dropdown from '../modules/table/tableDropdown';
import FORMAT from '../modules/table/tableFormats';
import BTable from '../modules/table';
import ModalForm from './modal';

import { HistoryDatePickerFROM } from './datepickerFROM';
import { HistoryDatePickerTO } from './datepickerTO';

import { getExls } from '../../scripts/actions';

class Table extends React.Component<any, any> {
    public filteredData: any =  {
        token: '',
        roads: [],
        date: {
            fromDate: '',
            toDate: ''
        },
    };

    state: any = {
        choosedRowData: [],
        fireUpdate: false,
        choosenRow: null,
        addDriver: false,
        canShow: false,
        loader: true,
        data: {},
        date: {
            fromDate: '',
            toDate: ''
        },
    };

    componentDidMount() {
        store.subscribe(STORE_LIST.ROAD, this);
    }
    
    componentWillUnmount() {
        store.unsubscribe(STORE_LIST.ROAD, this);
    }

    getDropdown = (cell: any, row: any) => (
        <Dropdown>
            <MenuItem eventKey="1" onClick={this.handleStop(row)}>Завершить</MenuItem>
            <MenuItem eventKey="2" onClick={this.handleEdit(row)}>Редактировать</MenuItem>
            <MenuItem eventKey="3" onClick={this.handleDriverEdit(row)}>Добавить водителя</MenuItem>
            <MenuItem eventKey="4" onClick={this.handleRemove(row)}>Удалить</MenuItem>
        </Dropdown>
    )

    handleSelectAll = (isSelected: any, rows: any) => {
        this.setState({
            choosedRowData: isSelected ? [...rows] : []
        });
    }

    handleDriverEdit = (row: any) => () => {
        this.setState({
            choosenRow: row,
            isEdit: true,
            addDriver: true,
        });
        this.props.setModal({ show: true });
    }

    handleMultiDelete = () => {
        const ids = this.state.choosedRowData.map((item: any) => item.id);
        if (ids.length > 0) {
            serverAction(ACTION_TYPES.ROAD.REMOVE, {
              id: ids
            }).then(() => {
                this.setState({
                    data: {
                        ...this.state.data,
                        roads: this.state.data.roads.filter(({ id }: any) => (!ids.includes(id))),
                    },
                    choosedRowData: []
                });
            });
        }
    }

    handleRemove = (row: any) => () => {
        serverAction(ACTION_TYPES.ROAD.REMOVE, {id: [row.id]})
            .then(() => {
                this.setState({
                    data: {
                        ...this.state.data,
                        roads: this.state.data.roads.filter((item: any) => item.id !== row.id)
                    },
                });
        });
    }

    handleEdit = (row: any) => () => {
        this.setState({
            choosenRow: row,
            isEdit: true,
            addDriver: false,
        });
        this.props.setModal({ show: true });
    }

    handleSelect = (row: any, isSelected: any) => {
        let { choosedRowData } = this.state;
        this.setState({
            choosedRowData: isSelected ?
            [...choosedRowData, row] : choosedRowData.filter(({ id }: any) => id !== row.id),
        });
    }

    handleAdd = () => {
        this.setState({
            isEdit: false,
            choosenRow: null,
            addDriver: false,
        }, () => {
            this.props.setModal({ show: true });
        });
    }

    handleStop = (row: any) => () => {
        serverAction(ACTION_TYPES.ROAD.UPDATE, {
            id: row.id,
            statusID: 3,
          }).then(() => {
            this.setState({
              data: {
                ...this.state.data,
                roads: this.state.data.roads.filter((item: any) => item.id !== row.id),
              },
            });
          });
    }

    handleMultiStop = () => {
      const drivers = this.state.choosedRowData.map((road: any) => ({
        driver_uid: road.driver.uid,
      }));
      const ids = this.state.choosedRowData.map(({ id }: any) => id);

      serverAction(ACTION_TYPES.ROAD.UPADATE_LIST, {
        drivers,
        statusID: 3,
      }).then(() => {
        this.setState({
          data: {
               ...this.state.data,
            roads: this.state.data.roads.filter(({ id }: any) => (!ids.includes(id))),
          },
          choosedRowData: []
        });
      });
    }

    dataLoad = () => {
        if (this.state.loader) {
            return (
                <SyncLoader
                    color={'#5898c6'}
                    loading={this.state.loader}
                    size={10}
                />
            );
        } else {
            return (
                'Нет маршрутов'
            );
        }
    }

    updateFromDate = (date: any) => {
        this.filteredData.date.fromDate = date.format('DD/MM/YYYY');

        this.setState({
            date: {
                fromDate: date.format('DD/MM/YYYY')
            }
        });
    }

    updateToDate = (date: any) => {
        this.filteredData.date.toDate = date.format('DD/MM/YYYY');

        this.setState({
            date: {
                toDate: date.format('DD/MM/YYYY')
            }
        });
    }

    exportExl = () => {
        getExls(this.filteredData);
    }

    afterSearch (filterObj: any, filteredTableData: any) {
        let dataLength = filteredTableData.length;
        if (dataLength !== 0) {
            this.filteredData = {
                ...this.filteredData,
                token: this.props.user.token,
                roads: filteredTableData,
            };
        }
    }

    render() {
        return (
            <div className="col-md-12">
                <div className="table--buttons">
                    <button className="addButton" onClick={this.handleAdd} />
                    <div
                        className={
                            (this.state.choosedRowData.length > 1) ?
                                'pull-right show' : 'pull-right hide'
                        }
                    >
                        <button className="finishButton" onClick={this.handleMultiStop} />
                        <button className="deleteButton" onClick={this.handleMultiDelete} />
                    </div>
                </div>

                <div className="col-md-12 history--buttons" style={{padding: 0}}>
                    <HistoryDatePickerFROM action={this.updateFromDate}/>
                    <HistoryDatePickerTO action={this.updateToDate}/>
                    <button className="exportButton" onClick={this.exportExl}>
                      Экспорт
                    </button>
                </div>

                <BTable
                    data={this.state.data && this.state.data.roads || []}
                    selectRow={{
                        mode: 'checkbox',
                        clickToSelect: false,
                        columnWidth: '38px',
                        className: 'checkbox--style',
                        onSelect: this.handleSelect,
                        onSelectAll: this.handleSelectAll,
                        disableSelectText: true,
                        customComponent: checkbox,
                    }}
                    options={{
                        edit: true,
                        noDataText: this.dataLoad(),
                        defaultSortName: 'updated',
                        defaultSortOrder: 'desc',
                        afterColumnFilter: (filterObj: any, filteredTableData: any) => 
                        this.afterSearch(filterObj, filteredTableData),
                    }}
                >
                  <TableHeaderColumn
                      columnClassName={FORMAT.ROW_CLASS}
                      dataField="updated"
                      hidden={true}
                  >
                      Дата
                  </TableHeaderColumn>
                    <TableHeaderColumn
                        columnClassName={FORMAT.ROW_CLASS}
                        dataField="driver"
                        className="driverLine"
                        filterFormatted={true}
                        width="100px"
                        dataFormat={FORMAT.DRIVER}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Водитель
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        columnClassName={FORMAT.ROW_CLASS}
                        dataField="sender_point"
                        width="100px"
                        dataFormat={FORMAT.SENDER_POINT}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Погрузка <br /> получ., пункт, дата
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="receiver_point"
                        width="100px"
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        dataFormat={FORMAT.RECEIVER_POINT}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Выгрузка <br />получ., пункт, дата
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="culture"
                        dataFormat={FORMAT.OBJECT}
                        className="cultureLine"
                        width="100px"
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Культура
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataFormat={FORMAT.WEIGHT}
                        width="100px"
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Вес <br/> отпр., прин., недостача
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="type"
                        width="80px"
                        dataFormat={FORMAT.OBJECT}
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Условие
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="contract"
                        className="contractLine"
                        width="90px"
                        dataFormat={FORMAT.OBJECT}
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Договор
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="ttn_number"
                        className="ttnLine"
                        width="155px"
                        columnClassName={FORMAT.ROW_CLASS}
                        dataFormat={FORMAT.STRING}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Номер ТТН
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="delivery_price"
                        className="priceLine"
                        width="82px"
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Цена поездки
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="transit_contract"
                        width="130px"
                        dataFormat={FORMAT.TRANSIT_CONTRACT}
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Транзит
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="status"
                        className="statusLine"
                        width="130px"
                        dataFormat={FORMAT.OBJECT}
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Статус
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        isKey={true}
                        dataField="id"
                        dataFormat={this.getDropdown}
                        width="38px"
                        columnClassName={FORMAT.ROW_CLASS}
                    />
                </BTable>

                <ModalForm
                    choosenRow={this.state.choosenRow}
                    modalInfo={this.state.data && this.state.data.info}
                    isEdit={this.state.isEdit}
                    addDriver={this.state.addDriver}
                />
            </div>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(Table);
