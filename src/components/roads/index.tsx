import * as React from 'react';
import Header from '../header/index';
import Table from './table';
// import * as socketIOClient from 'socket.io-client';

class Roads extends React.Component<any, any> {
    // state = {
    //     endpoint: 'http://localhost:4001',
    //     color: 'white'
    // };

    // send = () => {
    //     const socket = socketIOClient(this.state.endpoint);

    //     socket.emit('change color', this.state.color);
    //     console.log(this.state.color);
    //     // socket.emit('change color', 'red', 'yellow') | you can have multiple arguments
    // }

    // setColor = (color: any) => {
    //     this.setState({color: color});
    //     console.log(this.state.color);
    // }

    render() {
        // const socket = socketIOClient(this.state.endpoint);

        // socket.on('change color', (color: any) => {
        //     document.body.style.backgroundColor = color;
        // });

        return (
            <React.Fragment>
                <Header />
                {/* <div style={{ textAlign: 'center' }}>
                    <button onClick={() => this.send()}>Change Color</button>

                    <button id="white" onClick={() => this.setColor('white')}>White</button>
                    <button id="blue" onClick={() => this.setColor('blue')}>Blue</button>

                    <button id="red" onClick={() => this.setColor('red')}>Red</button>
                </div> */}
                <div className="col-md-12" style={{ padding: 0 }}>
                    <div className="col-md-12 zt--Container roadsTable">
                        <Table />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Roads;