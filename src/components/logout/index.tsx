// import * as React from 'react';
// import { connect } from 'react-redux';
// import { putActionsToProps, putStateProps } from '../../store/functions';
//
// class Logout extends React.Component <any, any> {
//     constructor(props: any) {
//         super(props);
//         // localStorage.clear();
//         // props.history.go('/');
//         // localStorage.removeItem('state');
//         // props.history.push('/');
//         props.location.pathname = '/';
//         console.log(props);
//     }
//
//     render() {
//         return (
//             <div />
//         );
//     }
// }
//
// export default connect(putStateProps, putActionsToProps)(Logout);

export const logOut = (props: any) => {
    props.setAuth({canAuth: false, token: ''});
    localStorage.removeItem('user');
};