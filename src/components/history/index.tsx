import * as React from 'react';
import Header from '../header/index';
import HistoryTable from './table';

export class History extends React.Component<any, any> {
    render() {
        return (
            <React.Fragment>
                <Header/>
                <div className="col-md-12" style={{padding: 0}}>
                    <div className="col-md-12 zt--Container">
                        <HistoryTable />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default History;
