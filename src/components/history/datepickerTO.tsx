import * as React from 'react';
import DatePicker from 'react-datepicker';
let moment = require('moment');

import 'react-datepicker/dist/react-datepicker.css';
import 'moment/locale/ru';
moment.locale('ru');

export class HistoryDatePickerTO extends React.Component <any, any> {

  constructor(props: any) {
    super(props);

    this.state = {
      startDate: null
    };
  }

  handleChange = (date: any) => {
    this.setState({
      startDate: date
    });

    this.props.action(date);
  }

  render () {
    return (
      <div className="datePicker--block">
      <DatePicker
        className="historyPicker"
        selected={this.state.startDate}
        onChange={value => (this.handleChange(value))}
      />
      <span>
        ПО
      </span>
    </div>
    );
  }
}