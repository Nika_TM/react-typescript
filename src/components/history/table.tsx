import * as React from 'react';
import { TableHeaderColumn } from 'react-bootstrap-table';
import { MenuItem } from 'react-bootstrap';
import { connect } from 'react-redux';

import { putActionsToProps, putStateProps } from '../../store/functions';
import { HistoryDatePickerFROM } from './datepickerFROM';
import { HistoryDatePickerTO } from './datepickerTO';
import Dropdown from '../modules/table/tableDropdown';
import FORMAT from '../modules/table/tableFormats';
import MapModal from './modals/map/index';
import { serverAction, ACTION_TYPES } from '../../scripts/actions';
import BTable from '../modules/table';
import { SyncLoader } from 'react-spinners';
import { getExls } from '../../scripts/actions';

class HistoryTable extends React.Component<any, any> {
    public filteredData: any =  {
        token: '',
        roads: [],
        date: {
            fromDate: '',
            toDate: ''
        },
    };
    
    state = {
        choosenRow: null,
        showMap: false,
        mapModal: false,
        loader: true,
        tableData: [],
    };

    updateFromDate = (date: any) => {
        this.filteredData.date.fromDate = date.format('DD/MM/YYYY');
    }

    updateToDate = (date: any) => {
        this.filteredData.date.toDate = date.format('DD/MM/YYYY');
    }

    componentDidMount() {
        serverAction(ACTION_TYPES.ROAD.HISTORY, null)
        .then((tableData: any) => {
            this.setState({
                tableData,
                loader: false,
            });
        });
    }

    getDropdown = (index: any, row: any) => (
      <Dropdown>
        <MenuItem eventKey="1" onClick={this.handleMapOpen.bind(null, row)}>Просмотреть путь</MenuItem>
      </Dropdown>
    )

    handleMapOpen = (row: any) => {
      this.setState({
        showMap: true,
        choosenRow: row,
      });
    }

    handleMapClose = () => {
      this.setState({
        showMap: false,
        choosenRow: null,
      });
    }

    dataLoad = () => {
        if (this.state.loader) {
            return (
                <SyncLoader
                    color={'#5898c6'}
                    loading={this.state.loader}
                    size={10}
                />
            );
        } else {
            return (
                'Нет маршрутов'
            );
        }
    }

    exportExl = () => {
        getExls(this.filteredData);
    }

    afterSearch (filterObj: any, filteredTableData: any) {
        let dataLength = filteredTableData.length;

        if (dataLength !== 0) {
            this.filteredData = {
                ...this.filteredData,
                token: this.props.user.token,
                roads: filteredTableData,
            };
        }
    }

    render() {
        return (
            <React.Fragment>
                <div className="col-md-12 history--buttons" style={{padding: 0}}>
                    <HistoryDatePickerFROM action={this.updateFromDate}/>
                    <HistoryDatePickerTO action={this.updateToDate}/>
                    <button className="exportButton" onClick={this.exportExl}>
                      Экспорт
                    </button>
                </div>

                <BTable
                    data={this.state.tableData}
                    options={{
                        noDataText: this.dataLoad(),
                        defaultSortName: 'updated',
                        defaultSortOrder: 'desc',
                        afterColumnFilter: (filterObj: any, filteredTableData: any) => 
                        this.afterSearch(filterObj, filteredTableData),
                    }}
                >
                  <TableHeaderColumn
                      columnClassName={FORMAT.ROW_CLASS}
                      dataField="updated"
                      hidden={true}
                  >
                      Дата
                  </TableHeaderColumn>
                    <TableHeaderColumn
                        columnClassName={FORMAT.ROW_CLASS}
                        dataField="driver"
                        className="driverLine"
                        filterFormatted={true}
                        width="120"
                        dataFormat={FORMAT.DRIVER}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Водитель
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        columnClassName={FORMAT.ROW_CLASS}
                        dataField="sender_point"
                        width="100px"
                        dataFormat={FORMAT.SENDER_POINT}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Погрузка <br/> получ., пункт, дата
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="receiver_point"
                        width="100px"
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        dataFormat={FORMAT.RECEIVER_POINT}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Выгрузка <br/>получ., пункт, дата
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="culture"
                        dataFormat={FORMAT.OBJECT}
                        className="cultureLine"
                        width="100px"
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Культура
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataFormat={FORMAT.WEIGHT}
                        width="100px"
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Вес <br/> отпр., прин., недостача
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="type"
                        width="80px"
                        dataFormat={FORMAT.OBJECT}
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Условие
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="contract"
                        className="contractLine"
                        width="150px"
                        dataFormat={FORMAT.OBJECT}
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Договор
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="carrier"
                        className="contractLine"
                        width="100px"
                        dataFormat={FORMAT.OBJECT}
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Перевозчик
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="ttn_number"
                        className="ttnLine"
                        width="80px"
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Номер ТТН
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        dataField="delivery_price"
                        className="priceLine"
                        width="82px"
                        columnClassName={FORMAT.ROW_CLASS}
                        filterFormatted={true}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Цена поездки
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="transit_contract"
                        className="statusLine"
                        width="130px"
                        dataFormat={FORMAT.TRANSIT_CONTRACT}
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Транзит
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="status"
                        className="statusLine"
                        width="130px"
                        dataFormat={FORMAT.OBJECT}
                        filterFormatted={true}
                        columnClassName={FORMAT.ROW_CLASS}
                        filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                    >
                        Статус
                    </TableHeaderColumn>

                    <TableHeaderColumn
                        isKey={true}
                        dataField="id"
                        dataFormat={this.getDropdown}
                        width="38"
                        columnClassName={FORMAT.ROW_CLASS}
                        className="dotsTd"
                    />
                </BTable>

                <MapModal
                  isOpen={this.state.showMap}
                  handleClose={this.handleMapClose}
                  choosenRow={this.state.choosenRow}
                />
            </React.Fragment>
        );
    }
}

export default connect <any, any, any>(putStateProps, putActionsToProps)(HistoryTable);
