import * as React from 'react';
import FormInput from '../../../../modules/form/formInput';

export class ShippingInformation extends React.Component <any, any> {
  render() {
    return (
      <div className="col-md-12">
      <div className="col-md-12 weight">
        <div className="col-md-6">
          <FormInput
            value={this.props.data.loadingWeight}
            name="loadingWeight"
            onChange={this.props.handleChange}
            placeholder="Вес отправки..."
          />
        </div>

        <div className="col-md-6">
          <FormInput
            value={this.props.data.unloadingWeight}
            placeholder="Вес получения..."
            name="unloadingWeight"
            onChange={this.props.handleChange}
          />
        </div>
      </div>

      <div className="col-md-12 pact">
          <div className="col-md-6 pact">
            <FormInput
              type="text"
              className="zt--input"
              placeholder="Номер договора..."
              name="contract"
              value={this.props.data.contract}
              onChange={this.props.handleChange}
            />
        </div>
          <div className="col-md-6 pact">
            <FormInput
              type="text"
              className="zt--input"
              placeholder="Номер ТТН..."
              name="ttnNumber"
              value={this.props.data.ttnNumber}
              onChange={this.props.handleChange}
            />
          </div>
      </div>

      <div className="col-md-12 price">
          <div className="col-md-6 pact">
            <FormInput
              type="text"
              className="zt--input"
              placeholder="Цена..."
              name="price"
              value={this.props.data.price}
              onChange={this.props.handleChange}
            />
          </div>
          <div className="col-md-6 pact">
            <FormInput
              type="text"
              name="distance"
              className="zt--input"
              value={this.props.data.distance}
              placeholder="Километраж.."
              onChange={this.props.handleChange}
            />
          </div>
      </div>
    </div>
    );
  }
}
