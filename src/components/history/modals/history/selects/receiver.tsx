import * as React from 'react';
import PointSelect from 'react-virtualized-select';
import FormSelect from '../../../../modules/form/formSelect';

export class SelectReciever extends React.Component < any, any > {
  constructor(props: any) {
    super(props);

    this.state = {
      receiver: {},
      pointDisabled: true,
      pointChoosed: ''
    };
    this.handleReceiver = this.handleReceiver.bind(this);
    this.handlePoint = this.handlePoint.bind(this);
  }

  handleReceiver(name: any, value: any) {
    if (value) {
      this.setState({
        [name]: value,
        pointDisabled: false
      });
      this.props.handleChange(name, value);
    } else {
      this.setState({
        senderPointDisabled: true
      });
    }
  }

  handlePoint(value: any) {
    this.setState({
      pointChoosed: value,
    }, () => {
      this.props.handleChange('receiverPoint', value);
    });
  }

  render() {
    const { data } = this.props;
    return (
      <div className="col-md-12">
        <div className="col-md-6">
          <FormSelect
            name="receiver"
            placeholder={this.state.receiver.name || 'Получатель'}
            options={data}
            onChange={this.handleReceiver}
            labelKey="name"
          />
        </div>

        <div className="col-md-6">
          <PointSelect
            name="reciever-point"
            className="zt--select pull-right"
            placeholder={this.state.pointChoosed.adress || 'Выгрузка'}
            disabled={this.state.pointDisabled}
            options={this.state.receiver.points}
            value={this.state.pointChoosed}
            onChange={this.handlePoint}
            labelKey="adress"
            noResultsText="Результатов нет"
          />
        </div>
      </div>
    );
  }
}
