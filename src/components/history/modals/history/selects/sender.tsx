import * as React from 'react';
import PointSelect from 'react-virtualized-select';
import FormSelect from '../../../../modules/form/formSelect';

export class SelectSender extends React.Component < any, any > {
  constructor(props: any) {
    super(props);

    this.state = {
      sender: {},
      pointDisabled: true,
      pointChoosed: ''
    };
    this.handleSender = this.handleSender.bind(this);
    this.handlePoint = this.handlePoint.bind(this);
  }

  handleSender(name: any, value: any) {
    if (value) {
      this.setState({
        [name]: value,
        pointDisabled: false
      });
      this.props.handleChange(name, value);
    } else {
      this.setState({
        pointDisabled: true
      });
    }
  }

  handlePoint(value: any) {
    this.setState({
      pointChoosed: value,
    }, () => {
      this.props.handleChange('senderPoint', value);
    });
  }

  render() {
    const { data } = this.props;
    return (
      <div className="col-md-12">
        <div className="col-md-6">
          <FormSelect
            name="sender"
            placeholder={this.state.sender.name || 'Отправитель'}
            options={data}
            onChange={this.handleSender}
            labelKey="name"
          />
        </div>

        <div className="col-md-6">
          <PointSelect
            name="sender-point"
            className="zt--select pull-right"
            placeholder={this.state.pointChoosed.adress || 'Погрузка'}
            disabled={this.state.pointDisabled}
            options={this.state.sender.points}
            value={this.state.pointChoosed}
            onChange={this.handlePoint}
            labelKey="adress"
            noResultsText="Результатов нет"
          />
        </div>
      </div>
    );
  }
}
