import * as React from 'react';
import { connect } from 'react-redux';
import { ButtonToolbar, Button, Modal } from 'react-bootstrap';

import { toast } from 'react-toastify';
import { FormDatePicker } from '../../../modules/form/formDatePicker';

import { putStateProps, putActionsToProps } from '../../../../store/functions';
import { ShippingInformation } from './selects/shippingInfo';
import { farmData, driverData, cultures } from '../../../modules/data';
import { SelectReciever } from './selects/receiver';
import FormSelect from '../../../modules/form/formSelect';
import { SelectSender } from './selects/sender';

import { MODAL_TYPE } from '../../../../scripts/constants';
import { getErrors } from '../../../modules/errors';

class ModalForm extends React.Component <any, any> {
    state: any = {
      culture: {},
      receiverPoint: {},
      senderPoint: {},
      loadingWeight: '',
      unloadingWeight: '',
      contract: '',
      ttnNumber: '',
      price: '',
      distance: '',
      driver: {},
    };

    static getDerivedStateFromProps(nextProps: any) {
      const data = nextProps.showModal.row;
      if (data) {
        return {
          receiver: data.receiver,
          receiverPoint: data.unloading,
          sender: data.sender,
          senderPoint: data.loading,
          culture: {
            name: data.culture,
          },
          loadingWeight: data.weight,
          unloadingWeight: data.weight,
          contract: data.contract,
          ttnNumber: data.ttnNumber,
          price: data.trip_price,
          distance: data.status,
          driver: data.driver,
        };
      }
      return null;
    }

    validate = () => {
      toast.dismiss();
      let isError = null;
      for (let key in this.state) {
        let errorName = getErrors({
          type: MODAL_TYPE.ROADS,
          fieldName: key,
          value: this.state[key]
        });

        if (errorName) {
          isError = true;
        }
      }
      return isError;
    }

    handleSave = () => {
      const isError = this.validate();
      if (!isError) {
        console.log('SUCCESS');
      }
    }

    handleHide = () => this.props.setModal({ show: false });

    handleChange = (name: any, value: any) => this.setState({ [name]: value });

    render() {
      return (
            <ButtonToolbar>
                <Modal
                    show={this.props.showModal.show}
                    onHide={this.handleHide}
                    dialogClassName="zt-modal"
                >
                    <Modal.Header closeButton={false}>
                        <Modal.Title id="contained-modal-title-lg">
                            {this.props.showModal.header ? this.props.showModal.header : 'Редактирование истории'}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="col-md-12">
                      <div className="col-md-12">
                        <h4>История маршрута</h4>
                      </div>
                      <div className="col-md-8">
                        <div className="col-md-12">

                          <div className="col-md-6">
                            <FormSelect
                              name="driver"
                              placeholder={this.state.driver.name || 'Водитель'}
                              options={driverData}
                              onChange={this.handleChange}
                            />
                          </div>

                          <div className="col-md-12">
                            <SelectSender data={farmData} handleChange={this.handleChange}/>
                            <SelectReciever data={farmData} handleChange={this.handleChange}/>
                          </div>
                          <div className="col-md-6">
                            <FormSelect
                              name="culture"
                              placeholder={this.state.culture.name || 'Культура'}
                              options={cultures}
                              onChange={this.handleChange}
                              labelKey="name"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="col-md-8">
                          <ShippingInformation data={{...this.state}} handleChange={this.handleChange}/>
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="col-md-8">
                          <FormDatePicker
                            onChange={this.handleChange}
                            name="date"
                            date={this.state.date}
                            text="Дата"
                            options={
                              {
                                popperPlacement: 'top-end',
                              }
                            }
                          />
                        </div>
                      </div>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button className="close--btn pull-right" onClick={this.handleHide}>Отменить</Button>
                        <Button className="save--btn pull-right" onClick={this.handleSave}>Сохранить</Button>
                    </Modal.Footer>
                </Modal>
            </ButtonToolbar>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(ModalForm);
