import * as React from 'react';
let moment = require('moment');

import { withGoogleMap, GoogleMap, withScriptjs, Polyline, Marker } from 'react-google-maps';
const gMapKey = 'https://maps.googleapis.com/maps/api/js?key=';
const keyEnd = 'AIzaSyAk6X_u5EbuT-EF1bo4kSfWQ6DFA_Ph-UQ&v=3.exp&libraries=geometry,drawing,places';

const defaultCoords = {
  lat: 49,
  lng: 31
};

const ModalGoogleMap = withScriptjs(withGoogleMap((props: any) => (
    <GoogleMap
      defaultCenter={
        new google.maps.LatLng(
          props.polylineCoords.accepted.length > 0
          ? props.polylineCoords.accepted[0].lat
          : defaultCoords.lat,
          props.polylineCoords.accepted.length > 0
          ? props.polylineCoords.accepted[0].lng
          : defaultCoords.lng)}
      defaultZoom={7}
      options={{
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: false
      }}
    >
      {
        props.polylineCoords.notAccepted && props.polylineCoords.notAccepted.length > 0 && (
          <React.Fragment>
            <Polyline
              path={props.polylineCoords.notAccepted}
              options={{
                      strokeColor: 'green',
                      strokeWeight: 2,
                      icons: [{
                                offset: '0',
                                repeat: '20px'
                            }],
                    }}
                    
            />
          <Marker
            position={props.polylineCoords.notAccepted[0]}
          />
          </React.Fragment>
        )
      }
      {
        props.polylineCoords.accepted && props.polylineCoords.accepted.length > 0 && (
          <React.Fragment>
            <Polyline
              path={props.polylineCoords.accepted}
              options={{
                      strokeColor: '#ff2527',
                      strokeWeight: 2,
                      icons: [{
                                offset: '0',
                                repeat: '20px'
                            }],
                    }}

            />
          <Marker
            position={props.polylineCoords.accepted[0]}
          />
          </React.Fragment>
        )
        
      }
    </GoogleMap>
)));

export default class MapModal extends React.Component <any, any> {
    static defaultProps = {
      choosenRow: {},
    };

    state = {
      isOpened: false,
      refs: false,
    };

    getPolyline = (coords: any) => (
      <Polyline
        path={coords}
        key={Date.now() + Math.random()}
      />
    )

    render() {
      if (this.props.isOpen) {
        document.getElementsByTagName('body')[0].classList.add('mapModalOverflow');
      } else {
        document.getElementsByTagName('body')[0].classList.remove('mapModalOverflow');
      }

      const data: any = this.props.choosenRow;
      const accepted = data && data.coordinates.accepted;
      const notAccepted = data && data.coordinates.notAccepted;
      
      return (
          this.props.isOpen && (
            <div className="map-modal">
              <ModalGoogleMap
                googleMapURL={gMapKey + keyEnd}
                loadingElement={<div style={{ height: '100%', width: '100%' }} />}
                containerElement={<div style={{ height: '100%', width: '100%' }} />}
                mapElement={<div style={{ height: '100%' }} />}
                polylineCoords={{
                  accepted,
                  notAccepted
                }}
              />
              <div className="map-modal-info">
                <div className="map-modal-header">Информация о заказе</div>
                <div className="map-modal-body">
                  <div className="map-modal-row">
                    <strong>Водитель:</strong> {data.driver && data.driver.name}</div>
                  <div className="map-modal-row">
                    <strong>Договор:</strong> {data.contract && data.contract.name}</div>
                  <div className="map-modal-row">
                    <strong>Пункт выгрузки:</strong> {data.receiver_point
                      && `${data.receiver_point.name}
                      ${moment(data.receiver_point.unloading_date).format('DD/MM/YYYY hh:mm')}`}</div>
                  <div className="map-modal-row">
                    <strong>Вес погрузки:</strong> {data.loading_weight}</div>
                  <div className="map-modal-row">
                    <strong>Пункт погрузки:</strong> {data.sender_point && data.sender_point
                      && `${data.sender_point.name}
                      ${moment(data.sender_point.loading_date).format('DD/MM/YYYY hh:mm')}`}</div>
                  <div className="map-modal-row">
                    <strong>Вес выгрузки:</strong> {data.unloading_weight}</div>
                  <div className="map-modal-row">
                    <strong>ТТН:</strong> {data.ttn_number}</div>
                  <div className="map-modal-row">
                    <strong>Транзит:</strong> {
                      (data.transit_contract && data.transit_contract.name) || 'Нет данных'}</div>
                </div>
              </div>
              <i
                onClick={this.props.handleClose}
                className="map-modal-close"
              />
            </div>
          )
        );
    }
}
