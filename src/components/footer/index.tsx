import * as React from 'react';

export default () => (
  <div className="col-lg-12 footer">
    <span className="copyright">Zlata logistics © 2018</span>
  </div>
);
