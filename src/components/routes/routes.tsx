import * as React from 'react';
import { HashRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Login from '../../components/login/loginform';
// import Dispatcher from '../../components/dispatcher';
import Contracts from '../../components/contracts';
import Transport from '../../components/transport';
import History from '../../components/history';
import Roads from '../../components/roads';
import Map from '../../components/map';
// import { SocketsFunc } from '../../scripts/sockets';

export const publicRoute = (
    <Router>
        <Switch>
            <Route exact={true} path="/" component={Login}/>
            {/* <Route path="/socket" component={SocketsFunc}/> */}
            <Redirect exact={true} from="/*" to="/" />
        </Switch>
    </Router>
);

export const privateRoute = (
    <Router>
        <Switch>
            {/* <Route path="/dispatchers" exact={true} component={Dispatcher}/> */}
            <Route path="/contracts" exact={true} component={Contracts}/>
            <Route path="/transport" exact={true} component={Transport}/>
            <Route path="/history" exact={true} component={History}/>
            <Route path="/roads" exact={true} component={Roads}/>
            <Route path="/map" exact={true} component={Map}/>
            <Redirect exact={true} from="/*" to="/roads"/>
        </Switch>
    </Router>
);
