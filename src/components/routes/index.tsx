import * as React from 'react';
import { privateRoute, publicRoute } from './routes';
import { connect } from 'react-redux';
import { putActionsToProps, putStateProps } from '../../store/functions';

class AuthInit extends React.Component<any, any> {
    constructor(props: any) {
        super(props);

        this.state = {
            canAuth: props.user.canAuth
        };
    }

    componentDidUpdate(previousProps: any) {
        if (previousProps.user.canAuth !== this.props.user.canAuth) {
            this.setState ({
                canAuth: this.props.user.canAuth
            });
        }
    }

    render() {
        return this.state.canAuth ? privateRoute : publicRoute;
    }
}

export default connect(putStateProps, putActionsToProps)(AuthInit);
