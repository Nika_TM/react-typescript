import * as React from 'react';
import { connect } from 'react-redux';
import { TableHeaderColumn } from 'react-bootstrap-table';
import { SyncLoader } from 'react-spinners';
import { serverAction, ACTION_TYPES } from '../../scripts/actions';
import FORMAT from '../modules/table/tableFormats';

import { putActionsToProps, putStateProps } from '../../store/functions';
import BTable from '../modules/table';

class Table extends React.Component<any, any> {
    state: any = {
      tableData: [],
      loader: true
    };

    componentDidMount() {
        serverAction(ACTION_TYPES.CARRIERS.LIST, null).then((tableData: any) => {
            this.setState({
                tableData,
                loader: false
            });
        });
    }

    dataLoad = () => {
        if (this.state.loader) {
            return (
                <SyncLoader
                    color={'#5898c6'}
                    loading={this.state.loader}
                    size={10}
                />
            );
        } else {
            return (
                'Нет маршрутов'
            );
        }
    }

    render() {
        return (
                <BTable
                    data={this.state.tableData}
                    options={{
                        noDataText: this.dataLoad(),
                        defaultSortName: 'carrier',
                    }}
                >
                  <TableHeaderColumn
                      dataField="carrier"
                      filterFormatted={true}
                      width="120"
                      dataFormat={FORMAT.OBJECT}
                      filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                  >
                      Перевозчик (ЧП)
                  </TableHeaderColumn>
                  <TableHeaderColumn
                      dataField="name"
                      filterFormatted={true}
                      filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                      dataFormat={FORMAT.TRANSPORT_DRIVER}
                      width="120"
                  >
                      Водитель
                  </TableHeaderColumn>
                  <TableHeaderColumn
                      dataField="auto"
                      width="120"
                      filterFormatted={true}
                      filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                      dataFormat={FORMAT.OBJECT}
                  >
                      Номер авто
                  </TableHeaderColumn>
                  <TableHeaderColumn
                      dataField="auto"
                      filterFormatted={true}
                      filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                      dataFormat={FORMAT.MODEL_NAME}
                      width="120"
                  >
                      Модель
                  </TableHeaderColumn>
                  <TableHeaderColumn
                      dataField="status"
                      className="driverLine"
                      filterFormatted={true}
                      filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                      dataFormat={FORMAT.DRIVER_STATUS}
                      width="120"
                      isKey={true}
                  >
                      Доступность
                  </TableHeaderColumn>
                </BTable>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(Table);
