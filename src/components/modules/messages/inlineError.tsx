import * as React from 'react';

const InlineError = ({text}: any) => <span className="alert alert-danger inlineError"> {text} </span>;

export default InlineError;
