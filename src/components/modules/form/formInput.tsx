import * as React from 'react';

export default class FormInput extends React.Component < any, any > {
  public static defaultProps = {
    type: 'text',
    name: 'name',
    placeholder: 'Введите данные',
    value: ''
  };

  state = {
    value: ''
  };

  static getDerivedStateFromProps(nextProps: any, prevState: any) {
    if (nextProps.value === prevState.value) {
      return null;
    }
    return {
      value: nextProps.value,
    };
   }

  handleChange = ({ target: { name, value }}: any) => {
    this.setState({
      value,
    }, () => {
      this.props.onChange(name, value);
    });
  }

  render() {
    return (
      <input
        className={`zt--input ${this.props.className}`}
        placeholder={this.props.placeholder}
        disabled={this.props.disabled}
        onChange={this.handleChange}
        value={this.state.value}
        {...this.props.options}
        type={this.props.type}
        name={this.props.name}
      />
    );
  }
}
