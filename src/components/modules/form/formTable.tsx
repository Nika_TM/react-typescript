import * as React from 'react';
const ToggleButton = require('react-toggle-button');
import FormInput from './formInput';

const toggleButtonOptions = {
  activeLabel: 'да',
  inactiveLabel: 'нет',
  trackStyle: {
    borderRadius: 0,
    border: '1px solid #d0d0d0',
  },
  thumbStyle: {
    borderRadius: 0,
    border: 'none',
    boxShadow: 'none',
  },
  colors: {
    active: {
      base: '#81c784',
    },
    inactive: {
      base: '#d0d0d0',
    }
  }
};

export default class FormTable extends React.Component < any, any > {
  handleToggle = (name: any, key: any) => {
    console.log(name, key);
    const points = [...this.props.farmPoints];
    points[key][name] = !points[key][name];
    this.props.handleChange({
      farmPoints: points
    });
  }

  handleChange = (name: any, value: any, key: any) => {
    const points = [...this.props.farmPoints];
    points[key].name = value;
    this.props.handleChange({
      farmPoints: points
    });
  }

  getRow = (point: any, key: any) => {
    return (
      <div className="row modal-table-row" key={key}>
        <div className="col-sm-6">
          <FormInput
            value={point.name}
            onChange={(name: any, value: any) => this.handleChange(name, value, key)}
          />
        </div>
        <div className="col-sm-2">
          <ToggleButton
            value={point.sender}
            {...toggleButtonOptions}
            onToggle={() => this.handleToggle('sender', key)}
          />
        </div>
        <div className="col-sm-2">
          <ToggleButton
            value={point.receiver}
            {...toggleButtonOptions}
            onToggle={() => this.handleToggle('receiver', key)}
          />
        </div>
        <div className="col-sm-2">
          <i
            className="remove-btn"
            onClick={() => this.props.handleRemove(key)}
          />
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="container-fluid modal-table">
       <div className="row modal-table-header">
         <div className="col-sm-6 text-center">Название</div>
         <div className="col-sm-2 text-center">Отправитель</div>
         <div className="col-sm-2 text-center">Получатель</div>
         <div className="col-sm-2 text-center">Удалить</div>
       </div>
       <div className="row modal-table-body">
         {this.props.farmPoints.map((farmPoints: any, key: any) => this.getRow(farmPoints, key))}
       </div>
     </div>
    );
  }
}
