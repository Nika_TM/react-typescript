import * as React from 'react';
import DatePicker from 'react-datepicker';
let moment = require('moment');

import 'react-datepicker/dist/react-datepicker.css';
import 'moment/locale/ru';
moment.locale('ru');

export class FormDatePicker extends React.Component <any, any> {
  constructor(props: any) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date: any) {
    this.props.onChange(this.props.name, date);
  }

  render () {
    return (
    <div className="datepicker">
      {this.props.text && (<span className="datePicker--text">{this.props.text}</span>)}
      <DatePicker
        className="datepicker--input"
        selected={this.props.date || moment()}
        onChange={this.handleChange}
        {...this.props.options}
      />
    </div>
    );
  }
}
