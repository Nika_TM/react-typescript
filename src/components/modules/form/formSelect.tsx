import * as React from 'react';
import Select from 'react-virtualized-select';

export default class FormSelect extends React.Component < any, any > {
  public static defaultProps = {
    data: {},
    labelKey: 'name',
    placeholder: '',
    noResultsText: 'Результатов нет',
    options: [],
    value: {
      name: false,
    }
  };

  handleChange = (value: any) => this.props.onChange(this.props.name, value);

  render() {
    return (
      <Select
        className={`zt--select ${this.props.className}`}
        noResultsText={this.props.noResultsText}
        placeholder={this.props.placeholder}
        {...this.props.editionOptions}
        labelKey={this.props.labelKey}
        onChange={this.handleChange}
        options={this.props.options}
        value={this.props.value}
        name={this.props.name}
        disabled={this.props.disabled}
      />
    );
  }
}
