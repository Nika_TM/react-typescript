export let farmData: any = [
    {
      id: 11,
      name: 'Andorra',
      points: [
        {
          id: 1,
          adress: 'Одесса ул.Василя Стуса 2б/12',
          isSender: true,
          isReceiver: false
        },
        {
          id: 2,
          adress: 'Одесса ул.Василя Стуса 2б/555',
          isSender: true,
          isReceiver: false
        },
        {
          id: 3,
          adress: 'Одесса ул.Василя Стуса 2б/1123',
          isSender: true,
          isReceiver: false
        }
      ]
    },
    {
      id: 12,
      name: 'Andorra2',
      points: [
        {
          id: 4,
          adress: 'Одесса ул.Василя Стуса 24б/1',
          isSender: true,
          isReceiver: false
        },
        {
          id: 5,
          adress: 'Одесса ул.Василя Стуса 13б/1125asd',
          isSender: true,
          isReceiver: false
        },
        {
          id: 6,
          adress: 'Одесса ул.Василя Стуса 92б/1xxas',
          isSender: true,
          isReceiver: false
        }
      ]
    },
 ];
 export let driverData: any = [
  {
    id: 12,
    name: 'Алексеев А.А. Фура Audi A8, XX0000XX'
  },
  {
    id: 111,
    name: 'Алексеев А.Б. Фура Audi A8, XX0000XX'
  },
  {
    id: 9,
    name: 'Алексеев А.В. Фура Audi A8, XX0000XX'
  },

  {
    id: 87,
    name: 'Алексеев А.Д. Фура Audi A8, XX0000XX'
  }
 ];

 export let cultures: any = [
   {name: 'Пшеница'},
   {name: 'Ячмень'},
   {name: 'Овёс'},
   {name: 'Рожь'},
   {name: 'Просо'},
   {name: 'Кукуруза'},
   {name: 'Полба'},
   {name: 'Гречиха'},
   {name: 'Киноа'},
 ];

 export const users = [
   {
       id: 1,
       name: 'Пользователь 1',
       phone: '0992334507',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 2,
       name: 'Пользователь 2',
       phone: '0992334505',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 3,
       name: 'Пользователь 3',
       phone: '0992334506',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 4,
       name: 'Пользователь 4',
       phone: '0992334504',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 5,
       name: 'Пользователь 5',
       phone: '0992334503',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 6,
       name: 'Пользователь 6',
       phone: '0992334500',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 7,
       name: 'Пользователь 7',
       phone: '0992334501',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 8,
       name: 'Пользователь 8',
       phone: '0992334502',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 9,
       name: 'Пользователь 9',
       phone: '0992334508',
       password: '123456',
       admin: 'Да'
   },
   {
       id: 10,
       name: 'Пользователь 10',
       phone: '0992334509',
       password: '123456',
       admin: 'Да'
   }
 ];
export const testData = {
  contracts: [
    {
      name: 'Контракт номер 1',
      uid: 312421,
      culture: {
        name: 'Зерно1',
      },
    },
    {
      name: 'Контракт номер 2',
      uid: 312421,
      culture: {
        name: 'Зерно2',
      },
    },
    {
      name: 'Контракт номер 3',
      uid: 312421,
      culture: {
        name: 'Зерно3',
      },
    },
    {
      name: 'Контракт номер 4',
      uid: 312421,
      culture: {
        name: 'Зерно4',
      },
    },
    {
      name: 'Контракт номер 5',
      uid: 312421,
      culture: {
        name: 'Зерно5',
      },
    },
  ],
  regions: [
    {
      name: 'Одесса',
      uid: '',
      points: [
        {
          name: 'Кацапетовка',
        }
      ]
    },
    {
      name: 'Киев',
      uid: ''
    },
    {
      name: 'Николаев',
      uid: ''
    },
  ],
};

export const getRoadsHistory = (quantity: any) => {
    const roadsData = [];
    const startId = roadsData.length;

    for (let i = 0; i < quantity; i++) {
        const id = startId + 100 + i;

        roadsData.push({
            id: id,
            driver: {
              name: 'Алексеев А.А. ' + i + 'х00' + i + '0хх',
              phone: ' 096123456' + i
            },
            reciever: {
              name: 'name'
            },
            sender: {
              name: 'name'
            },
            loading: {
                name: 'Злата-трейд',
                adress: 'Василия Стуса 2б',
                time: 'Сегодня в 10:00'
            },
            unloading: {
                name: 'Злата-трейд',
                adress: 'Василия Стуса 2б/1',
                time: 'Сегодня в 11:00'
            },
            culture: 'Зерно',
            weight: '10000 т.',
            deficit: '10' + i + ' т.',
            contract: '№1-1231231231 от 10.20.201' + i,
            ttnNumber: '12345678912345678' + i,
            price: '1000' + i + 'грн.',
            status: '12' + i + '.5 км.',
            trip_price: '10' + i + '00 грн.',
        });
    }
    return roadsData;
};
