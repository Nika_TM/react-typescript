import * as React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { putActionsToProps, putStateProps } from '../../../store/functions';
import { connect } from 'react-redux';

class BTable extends React.Component<any, any> {
    state = {
        data: []
    };
    id = 0;
    static getDerivedStateFromProps(nextProps: any) {
      return {
        data: nextProps.data,
      };
    }
    
    renderSizePerPageDropDown = (props: any) => {
      return (
        <div className="btn-group">
          {
            [ 10, 25, 30 ].map((n, idx) => {
              const isActive = (n === props.currSizePerPage) ? 'active' : '';
              return (
                <button
                  key={idx}
                  type="button"
                  className={`btn btn-default ${isActive}`}
                  onClick={() => props.changeSizePerPage(n)}
                >{n}
                </button>
              );
            })
          }
        </div>
      );
    }
    
    handleChange = () => {
      this.id = 0;
    }

    render() {
        const options: any = {
            sizePerPageList: [10, 25, 30],
            sizePerPage: 10,
            noDataText: 'В таблице нет Маршрутов',
            defaultSortName: 'id',
            defaultSortOrder: 'asc',
            sizePerPageDropDown: this.renderSizePerPageDropDown,
            afterTableComplete: this.handleChange,
            ...this.props.options
        };

        return (
            <BootstrapTable
                data={this.state.data}
                pagination={true}
                options={options}
                selectRow={this.props.selectRow}
            >
              <TableHeaderColumn
                  width="40px"
                  dataFormat={() => {
                    this.id = this.id + 1;
                    return String(this.id);
                  }}
              >
                №
              </TableHeaderColumn>
                {this.props.children}
            </BootstrapTable>
        );
    }
}

export default connect <any, any, any> (putStateProps, putActionsToProps)(BTable);
