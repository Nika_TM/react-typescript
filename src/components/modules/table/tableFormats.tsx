import { CONTRACT_STATUS, DRIVER_STATUS } from '../../../scripts/constants';
let moment = require('moment');

const summWeight = (loading: number, unloading: number) => {
  let summ = loading - unloading;
  if (loading < unloading) {
    return -summ;
  } else {
    return -+summ;
  }
};

 const FORMAT = {
  PHONE: (cell: any, row: any) => {
    return '<a href="tel:+' + row.phone || '-' + '">' + row.phone || '-' + ' </a>';
  },

  SELF_PHONE: (cell: any, row: any) => {
    return '<a href="tel:+' + row.personalPhone || '-' + '">' + row.personalPhone  || '-' + ' </a>';
  },

  ROW_CLASS: (fieldValue: any, row: any, rowIdx: any, colIdx: any) => {
      if (rowIdx % 2 === 0) {
          if (colIdx % 2 === 1) {
              return 'td-light';
          } else {
              return 'td-medium';
          }
      } else {
          if (colIdx % 2 === 1) {
              return 'td-medium';
          } else {
              return 'td-dark';
          }
      }
  },

  RECEIVER_POINT: (cell: any, row: any) => {
    
    return cell &&
    `${cell.name}</br>${row.unloading_date ? moment(row.unloading_date).format('DD/MM/YYYY H:MM') : 'Нет даты'}`;
  },

  SENDER_POINT: (cell: any, row: any) => {
    return cell &&
    `${cell.name}</br>${row.loading_date ? moment(row.loading_date).format('DD/MM/YYYY H:MM') : 'Нет даты'}`;
  },

  WEIGHT: (cell: any, row: any) => 
  `${row.loading_weight || '-'}<br/>${row.unloading_weight || '-'} <br/>
  ${(row.loading_weight && row.unloading_weight && summWeight(row.loading_weight, row.unloading_weight))}`,

  DEFICIT: (cell: any, row: any) => {
      if (row) {
        return `<p>${row.loading_weight - row.unloading_weight}</p>`;
      }
      return '';
  },

  OBJECT: (cell: any, row: any) => {
    return (cell && cell.name) || 'Нет данных';
  },  

  TRANSIT_CONTRACT: (cell: any, row: any) => {
    return (cell && cell.name) || 'Нет данных';
  },

  DRIVER: (cell: any, row: any) => {
      if (row && row.driver && row.driver.name) {
        return `${row.driver.name || '-'}</br><a href="tel:+${row.driver.phone || '-'}">${row.driver.phone || '-'}</a>`;
      } else {
        return 'Нет данных';
      }
    }
  ,

  TRANSPORT_DRIVER: (cell: any, row: any) => {
      if (row) {
        return `${row.name || 'Нет данных'}</br><a href="tel:+${row.phone || '-'}">${row.phone || '-'}</a>`;
      } else {
        return 'Нет данных';
      }
    }
  ,

  LOADING: (cell: any, row: any) => (
    `<p>${row.loading.name}</p>
    <p>${row.loading.adress}</p>
    <p>${row.loading.time}</p>`
  ),

  CONTRACT_STATUS: (statusID: any) => CONTRACT_STATUS[statusID] || 'Нет данных',

  UNLOADING: (cell: any, row: any) => (
    `<p>${row.unloading.name}</p>
    <p>${row.unloading.adress}</p>
    <p>${row.unloading.time}</p>`
  ),
  DRIVER_STATUS: (statusID: any) => DRIVER_STATUS[statusID] || 'Нет данных',

  STRING: (str: any) => str || 'Нет данных',

  MODEL_NAME: (cell: any) => cell && cell.model_name || 'Нет данных',
};

export default FORMAT;
