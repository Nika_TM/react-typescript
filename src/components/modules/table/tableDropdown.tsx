import * as React from 'react';
import { DropdownButton } from 'react-bootstrap';

const Dots = () => (
  <i/>
);

export default (props: any) => (
  <span className="dropDownTable">
      <DropdownButton
          bsStyle="default"
          title={<Dots/>}
          noCaret={true}
          id="dropdown-no-caret"
      >
          {props.children}
      </DropdownButton>
  </span>
);
