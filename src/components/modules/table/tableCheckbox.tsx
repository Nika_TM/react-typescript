import * as React from 'react';

export default (props: any) => {
    const { type, checked, disabled, onChange, rowIndex } = props;
    if (rowIndex === 'Header') {
        return (
            <div className="checkbox-personalized">
                <label className="custom-checkbox-header-container" htmlFor={'checkbox' + rowIndex}>
                    <input
                        className="header-checkbox"
                        type={type}
                        name={'checkbox' + rowIndex}
                        id={'checkbox' + rowIndex}
                        checked={checked}
                        disabled={disabled}
                        onChange={e => onChange(e, rowIndex)}
                        ref={
                            input => {
                                if (input) {
                                    input.indeterminate = props.true;
                                }
                            }
                        }
                    />
                    <span className="checkmark"/>
                </label>
            </div>
        );
    } else {
        return (
            <div className="checkbox-personalized">
                <label className="custom-checkbox-container" htmlFor={'checkbox' + rowIndex}>
                    <input
                        type={type}
                        name={'checkbox' + rowIndex}
                        id={'checkbox' + rowIndex}
                        checked={checked}
                        disabled={disabled}
                        onChange={e => onChange(e, rowIndex)}
                        ref={
                            input => {
                            if (input) {
                                input.indeterminate = props.indeterminate;
                            }
                        }}
                    />
                    <span className="checkmark"/>
                </label>
            </div>
        );
    }
};
