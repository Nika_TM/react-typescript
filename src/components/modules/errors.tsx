import * as React from 'react';
import { toast } from 'react-toastify';
import { MODAL_TYPE } from '../../scripts/constants';

/**
 * [getErrors description]
 * @param  {[String]} type      Modal name
 * @param  {[String]} fieldName Name of testing field
 * @param  {[Any]} value        Value of testing field
 * @return {[Any]}              Errored fieldName or false
 */
export const getErrors = ({type, fieldName, value}: any) => {
  let error: any;

  switch (type) {
    case MODAL_TYPE.ROADS: error = getRoadsError(fieldName, value);
    break;
    default: error = false;
  }

  if (error) {
    showError(error);
    return fieldName;
  }
};

function getRoadsError(name: string, value: any) {
  switch (name) {
    case 'cerrier': return !isExistObj(value) && 'Выберите транспортную компанию';
    case 'contract': return !isExistObj(value) && 'Выберите контракт';
    case 'selectedAutos': {
      if (value.length === 0 ) {
        return 'Заполните колонку водителей';
      }
      let message: any = false;
      value.forEach((transport: any) => {
        if (!transport.carrier) {
          message = 'Добавьте транспортную компанию';
        } else if (!transport.auto) {
          message = 'Добавьте автомобиль';
        } else if (!transport.driver) {
          message = 'Добавьте водителя';
        }
      });
      return message;
    }
    case 'receiver': {
      if (!isExistObj(!value || value.region)) {
        return 'Выберите регион получателя';
      }
      if (!isExistObj(!value || value.point)) {
        return 'Выберите пункт получателя';
      }
      return false;
    }
    case 'sender': {
      if (!isExistObj(!value || value.region)) {
        return 'Выберите регион отправителя';
      }
      if (!isExistObj(!value || value.point)) {
        return 'Выберите пункт отправителя';
      }
      return false;
    }
    default: return false;
  }
}

// function isEmptyStr(str: any) {
//   return !str;
// }

function isExistObj(obj: any) {
  return obj && Object.keys(obj).length > 0;
}

function showError(error: any) {
  toast.error(
  <div>
      <h4>ОШИБКА!</h4>
      <p>{error}</p>
  </div>,
  {
      position: toast.POSITION.TOP_RIGHT,
      className: 'error-message',
      autoClose: 8000,
      draggable: false,
      pauseOnHover: true,
      hideProgressBar: true
  });
}
