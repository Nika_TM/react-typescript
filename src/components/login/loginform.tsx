import * as React from 'react';
import { FormGroup, FormControl } from 'react-bootstrap';
import { Grid, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux';

import { putStateProps, putActionsToProps } from '../../store/functions';
import InlineError from '../modules/messages/inlineError';
const AuthLogo = require('../../assets/images/logo.png');
import { login } from '../../scripts/actions';

class LoginForm extends React.Component<any, any> {
    state = {
        user: {
            phone_number: '',
            password: ''
        },
        errors: {
            text: ''
        }
    };

    getLoginState = () => {
        let length = this.state.user.phone_number.toString().length;
        let truePhone: string = '';

        if (length === 11) {
            truePhone = this.state.user.phone_number.slice(0, this.state.user.phone_number.length - 1);
            this.state.user.phone_number = truePhone;
            return 'success';
        }
        if (length === 10) {
            return 'success';
        }
        if (length > 0 && length < 10) {
            return 'error';
        }
        return null;
    }

    onChange = (e: any) => {
        if (e.target.name === 'phone') {
            this.setState({user: {...this.state.user, [e.target.name]: e.target.value.replace(/\D/g, '')}});
        } else {
            this.setState({user: {...this.state.user, [e.target.name]: e.target.value}});
        }
    }

    onPaste = (e: any) => {
        e.preventDefault();
    }

    onSubmit = (e: any) => {
        e.preventDefault();
        if (this.state.user.phone_number === '' || this.state.user.password === '') {
            this.setState({
                errors: {
                    text: 'Введите логин и пароль'
                }
            });
        } else {
            login(this.state.user).then(
                res => {
                    if (res.data.status === 'success') {
                        this.props.setAuth({canAuth: true, token: res.data.data.token});
                        this.props.history.push('/');
                    } else {
                        this.setState({
                            errors: {
                                text: res.data.message
                            }
                        });
                    }
                }
            );
        }
    }

    render() {
        const errors = this.state.errors;
        return (
            <section className="authorization">
                <Grid>
                    <Row className="show-grid">
                        <Col xs={12} md={12} className="auth--logo">
                            <img src={AuthLogo}/>
                        </Col>

                        <Col xs={12} md={5} className="auth--form">
                            <form className="col-xl-12 col-md-12 col-sm-12 col-xs-12" onSubmit={this.onSubmit}>
                                <FormGroup validationState={this.getLoginState()}>
                                    <FormControl
                                        type="text"
                                        value={this.state.user.phone_number}
                                        placeholder="Логин"
                                        name="phone_number"
                                        onChange={this.onChange}
                                        autoComplete="off"
                                        onPaste={this.onPaste}
                                    />
                                    <FormControl.Feedback/>
                                </FormGroup>

                                <FormGroup
                                    className="no--margin"
                                >
                                    <FormControl
                                        type="password"
                                        name="password"
                                        placeholder="Пароль"
                                        defaultValue={this.state.user.password}
                                        onChange={this.onChange}
                                        autoComplete="off"
                                        onPaste={this.onPaste}
                                    />
                                    <FormControl.Feedback/>
                                </FormGroup>

                                <button className="auth--button">Войти</button>
                            </form>
                            {errors.text && <InlineError text={errors.text}/>}
                        </Col>
                    </Row>
                </Grid>
            </section>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(LoginForm);
