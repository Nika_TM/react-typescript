import * as React from 'react';
import Header from '../header/index';
import Table from './table';
import { putStateProps, putActionsToProps } from '../../store/functions';
import { connect } from 'react-redux';

export class Dispatcher extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <Header/>
                <div className="col-md-12" style={{padding: 0}}>
                    <div className="col-md-12 zt--Container">
                        <Table/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(Dispatcher);
