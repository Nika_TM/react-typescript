import * as React from 'react';
import { TableHeaderColumn } from 'react-bootstrap-table';
import { putActionsToProps, putStateProps } from '../../store/functions';
import { connect } from 'react-redux';

import FORMAT from '../modules/table/tableFormats';
import BTable from '../modules/table';
import { users } from '../modules/data';

class Table extends React.Component<any, any> {
    render() {
        return (
              <BTable
                  data={users}
                  options={{ defaultSortOrder: 'asc' }}
              >
                <TableHeaderColumn
                    isKey={true}
                    dataField="id"
                    hidden={true}
                />
                <TableHeaderColumn
                    columnClassName={FORMAT.ROW_CLASS}
                    dataField="name"
                    className="driverLine"
                    width="189px"
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                >
                    ФИО
                </TableHeaderColumn>

                <TableHeaderColumn
                    className="phoneLine"
                    width="150px"
                    dataField="phone"
                    dataFormat={FORMAT.PHONE}
                    columnClassName={FORMAT.ROW_CLASS}
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                >
                    Номер телефона
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="password"
                    className="cultureLine"
                    width="251px"
                    columnClassName={FORMAT.ROW_CLASS}
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                >
                    Пароль
                </TableHeaderColumn>

                <TableHeaderColumn
                    dataField="admin"
                    className="weightLine"
                    width="260px"
                    columnClassName={FORMAT.ROW_CLASS}
                    filter={{ type: 'TextFilter', placeholder: 'Поиск', delay: 500 }}
                >
                  Администратор
                </TableHeaderColumn>
              </BTable>
        );
    }
}

export default connect(putStateProps, putActionsToProps)(Table);
