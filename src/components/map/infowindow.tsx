import * as React from 'react';
const { Marker } = require('react-google-maps');
const { InfoBox } = require('react-google-maps/lib/components/addons/InfoBox');

const zIcon = require('../../assets/images/zIcon.png');

export class InfoWindow extends React.Component <any, any> {
  state = {
    showed: false,
    animation: null
  };

  openInfo: any = () => {
    if (this.state.showed === false) {
      this.props.handleOpen(this.props.marker);
      this.setState({
        showed: true,
        animation: google.maps.Animation.BOUNCE
      });
    } else {
      this.setState({
        showed: false,
        animation: null
      });
    }
  }

  render() {
     const position = {
       lat: this.props.marker.lat,
       lng: this.props.marker.lng
     };
     let options: any = {};
     options.icon = this.props.marker.status === 0 ? null : {
      url: zIcon,
      scaledSize: new google.maps.Size(23, 36),
     };
      return (
        <div className="markerOuter">
          <Marker
            key={position.lat}
            onClick={this.openInfo.bind(this.props.marker)}
            position={position}
            options={options}
            noRedraw={true}
            animation={this.state.animation}
          >
            <InfoBox
              defaultPosition={position}
              onClick={this.openInfo.bind(this.props.marker)}
              options={{
                closeBoxURL: '',
                boxClass: 'road_title',
                enableEventPropagation: true,
                disableAutoPan: true,
              }}
            >
              <div
                key={this.props.marker.lat}
                onClick={this.openInfo.bind(this.props.marker)}
                style={{
                  backgroundColor: '#ffffff',
                  border: '1px solid #212426',
                  padding: '0px 12px',
                  lineHeight: 2,
                  width: '100%',
                  textOverflow: 'ellipsis',
                  whiteSpace: 'nowrap'
                }}
              >
                {`${this.props.data.auto_name || 'Нет номера автомобиля'} ${this.props.data.auto_model || ''}`}
              </div>
            </InfoBox>

            {this.state.showed && <InfoBox
              zIndex={1}
              options={{
                closeBoxURL: '',
                enableEventPropagation: true,
                boxClass: 'road_info',
                disableAutoPan: true,
              }}
            >
              <div style={{ backgroundColor: '#3b3b3b', padding: '12px 0'}}>
                <h2 className="road_info_title">Информация о заказе</h2>
                <div className="col-md-12 popupInfo">
                  <p><strong>Водитель:</strong> {this.props.data.driver_name || 'Нет данных'}</p>
                  <p><strong>Номер машины:</strong> {this.props.data.auto_name || 'Нет данных'}</p>
                  <p><strong>Договор:</strong> {this.props.data.contract_name || 'Нет данных'}</p>
                  <p><strong>ТТН:</strong> {this.props.data.ttn_number || 'Нет данных'}</p>
                  <p><strong>Культура:</strong> {this.props.data.culture_name || 'Нет данных'}</p>
                  <p><strong>Погрузка:</strong> {this.props.data.loading_point || 'Нет данных'}</p>
                  <p><strong>Вес погрузки:</strong> {this.props.data.loading_weight || 'Нет данных'}</p>
                  <p><strong>Выгрузка:</strong> {this.props.data.unloading_point || 'Нет данных'}</p>
                  <p><strong>Вес выгрузки:</strong> {this.props.data.unloading_weight || 'Нет данных'}</p>
                </div>
              </div>
            </InfoBox> }
          </Marker>
        </div>
      );
    }
  }
