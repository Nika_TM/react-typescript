import * as React from 'react';
import { connect } from 'react-redux';
import { withGoogleMap, GoogleMap, withScriptjs } from 'react-google-maps';

import { putActionsToProps, putStateProps } from '../../store/functions';
import { store, STORE_LIST } from '../../scripts/store';
import { InfoWindow } from './infowindow';

const keyEnd = 'AIzaSyAk6X_u5EbuT-EF1bo4kSfWQ6DFA_Ph-UQ&v=3.exp&libraries=geometry,drawing,places';
const gMapKey = 'https://maps.googleapis.com/maps/api/js?key=';

const GoogleMapComponent = withScriptjs(withGoogleMap((props: any) => (
    <GoogleMap
      defaultCenter={new google.maps.LatLng(49, 31)}
      defaultZoom={7}
      options={{
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        zoomControl: false,
      }}
      ref={(ref: any) => { props.getMapRef(ref); }}
    >
      {
        props.data.map((marker: any, index: any) => (
          <InfoWindow
            marker={marker.last_coordinates}
            data={marker}
            key={index}
            handleOpen={props.clickedInfoBox}
            ref={(infoWinRef: any) => props.getInfoBoxRef(infoWinRef)}
          />
        ))
      }
    </GoogleMap>
 )));

class Gmap extends React.Component <any, any, any> {
  state: any = {
      mapMarkers: [],
      mapRef: [],
      data: []
  };

  mapRef: any = null;
  mapMarkers: any = [];

  componentDidMount() {
      store.subscribe(STORE_LIST.MAP, this);
  }

  componentWillUnmount() {
      store.unsubscribe(STORE_LIST.MAP, this);
  }

  closeAll = () => {
    if (!this.state) {
      this.mapMarkers.forEach((item: any) => {
        if (item.state.showed === true) {
          item.setState({
            showed: false,
            animation: null
          });
        }
      });
    }
  }

  clickedInfoBox = (marker: any) => {
    if (marker) {
      this.mapRef.panTo({
        lat: marker.lat,
        lng: marker.lng
      });

      this.mapMarkers
        .forEach((item: any) => {
          if (item && item.state && item.state.showed === true) {
            item.setState({
              showed: false,
              animation: null
            });
          }
        });
    }
  }

  getRoads = () => this.state.data && this.state.data.filter((road: any) => road.last_coordinates) || [];

  getInfoBoxRef = (reactElemnt: any) => this.mapMarkers.push(reactElemnt);

  getMapRef = (mapRef: any) => this.mapRef = mapRef;

  render() {
    return (
      <GoogleMapComponent
        data={this.getRoads()}
        clickedInfoBox={this.clickedInfoBox}
        getInfoBoxRef={this.getInfoBoxRef}
        getMapRef={this.getMapRef}
        googleMapURL={gMapKey + keyEnd}
        loadingElement={<div style={{ height: '95vh', width: '100%' }} />}
        containerElement={<div style={{ height: '90vh', width: '100%' }} />}
        mapElement={<div style={{ height: '90vh' }} />}
      />
    );
  }
}

export default connect(putStateProps, putActionsToProps)(Gmap);
