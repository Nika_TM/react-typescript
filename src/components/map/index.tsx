import * as React from 'react';
import Header from '../header/index';
import Gmap from './map';

export class Map extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
    }

    render(): any {
        return (
            <div>
                <Header/>
                <div className="col-md-12" style={{padding: 0, marginTop: '-20px'}}>
                  <Gmap />
                </div>
            </div>
        );
    }
}

export default Map;
