import { EventEmitter } from 'events';

const channel = new EventEmitter();

const POSSIBLE_EVENTS = {
    ROADS: 'roads',
    MAP: 'roads_map'
};

export {
    channel,
    POSSIBLE_EVENTS,
};