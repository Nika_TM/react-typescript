export const MODAL_TYPE = {
  ROADS: 'ROADS',
  HISTORY: 'HISTORY',
};

const API_MODE = {
  DEVELOPMENT: 'http://logic.comnd-x.com',
  PRODUCTION: 'http://zt-logic.com',
};

export const API_URL = API_MODE.PRODUCTION;

export const ROAD_TYPE = {
  EXW: 1,
  SPT: 2,
};

export const CONTRACT_STATUS = {
  1: 'Новый',
  2: 'В работе',
  3: 'Завершен',
};

export const DRIVER_STATUS = {
  0: 'Свободен',
  1: 'В работе',
};

export const ROAD_STATUS = {
  1: 'Новый',
  2: 'Принят',
  3: 'Завершен',
  4: 'Завершен водителем',
};
