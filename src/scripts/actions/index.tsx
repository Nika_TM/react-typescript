import axios from 'axios';
import * as React from 'react';
import { toast } from 'react-toastify';
declare const Promise: any;

import { channel } from '../events';

import { store } from '../store';

import { API_URL } from '../constants';

import { database } from '../../firebase/';

const  defaultPromise = new Promise((resolve: any) => resolve());

const API = 'api';

export const ACTION_TYPES: any = {
  CONTRACT: {
    LIST: 'contract/list'
  },
  CARRIERS: {
    LIST: 'carriers/list'
  },
  ROAD: {
    LIST: 'road/list',
    MAP: 'road/map_info',
    REMOVE: 'road/delete',
    CREATE: 'road/create',
    UPDATE: 'road/update',
    UPADATE_LIST: 'road/update_list',
    HISTORY: 'road/history'
  },
};

export function serverAction(ACTION_TYPE: string, data: any) {
  data = data || {};

  const token = getToken();
  if (token) {
    data.token = token;
    return axios.post(`${API_URL}/${API}/${ACTION_TYPE}`, data).then((res: any) => {
      return validate(res);
    });
  }

  return defaultPromise;
}

export const login = (data: any) => (
  axios.post(`${API_URL}/api/auth`, {login: data.phone_number, password: data.password})
);

function getToken() {
  const serializeState = localStorage.getItem('storage');
  if (serializeState === null) {
    return showError(null);
  }
  const token = JSON.parse(serializeState).user.token;
  if (!token) {
    return showError(null);
  }
  return token;
}

function validate(res: any) {
    if (res.status === 200 && res.data.status === 'success') {
      return res.data.data;
    } else {
      showError(res.data.message);
      return defaultPromise;
    }
}

function showError(message: any) {
  const errorOptions: any = {
      onClose: () => {
          localStorage.clear();
          window.location.reload();
      },
      position: toast.POSITION.TOP_CENTER,
      className: 'error-warning',
      autoClose: 2000,
      draggable: false,
      pauseOnHover: true,
      hideProgressBar: true,
  };

  toast.warn(
      <div>
          <h4>{message || 'В доступе отказанно!'}</h4>
      </div>,
  errorOptions);
  return null;
}

database.ref('/').on('child_changed', (snapshot: any) => {
  if (snapshot.key) {
    channel.emit(snapshot.key);
    store.on(snapshot.key);
  }
});

export const getExls = (data: any) => {
  axios.post('http://zt-logic.com/api/xls', data)
  .then(function (response: any) {
      window.open(response.data.data, '_self');
  })
  .catch(function (error: any) {
      console.log(error);
  });
};