import { EventEmitter } from 'events';
import { serverAction, ACTION_TYPES } from './actions';

const STORE_LIST = {
    ROAD: 'roads',
    MAP: 'road_map',
};

class Store {
    events: any;
    storages: any;
    constructor() {
        this.events = new EventEmitter();

        this.storages = Object.keys(STORE_LIST).map((key) => ({
                [name]: null,
            }
        ));

        this.events.on('changeData', this.handleServerAction());
    }

    handleServerAction = () => (actionName: any) => {
        this.handleList(actionName);
    }

    handleList = (storeName: string) => {
        let action = '';

        switch (storeName) {
            case STORE_LIST.ROAD: action = ACTION_TYPES.ROAD.LIST;
            break;
        default: action = ACTION_TYPES.ROAD.MAP;
        }

        return serverAction(action, null)
            .then((data: any) => {
                this.storages[storeName] = data;
                this.events.emit(storeName, storeName);
            });
    }

    on = (actionName: any) => {
        this.events.emit('changeData', actionName);
    }

    componentHandler = (component: any) => (storeName: any) => {
        component.setState({
            data: this.storages[storeName],
        });
    }
    
    initComponentStore = (storeName: string, component: any) => {
        component.setState({
            data: this.storages[storeName],
        });
    }

    subscribe = (storeName: string, component: any) => {
        this.events.on(storeName, this.componentHandler(component));

        this.initComponentStore(storeName, component);

        if (!this.storages[storeName]) {
            this.handleList(storeName);
        }
    }

    unsubscribe = (storeName: string, component: any) => {
        this.events.removeAllListeners(storeName);
    }
}

const store = new Store;

export {
    STORE_LIST,
    store
};
